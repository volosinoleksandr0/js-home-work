let selectedElement;
var selectedMenu = "menu-count";
//масиві для хран данних
let rateNBY = [];
let arrCounts = [];

//созд шаблоні для будущих об
class Count{
    constructor(name, color, EUR, UAH, USD, note, id) {
        this.name = name;
        this.color = color;
        this.UAH = UAH;
        this.EUR = EUR;
        this.USD = USD;
        this.note = note;
        this.id = id;
    }   
    getAllIncome(){
        let sumUAH = 0;
        let sumEUR = 0;
        let sumUSD = 0;
        this.income.filter((x)=>{
            switch (x.currency) {
                case "UAH":
                    sumEUR += x.calculate();
                    break;
                case "EUR":
                    sumUAH += x.calculate();
                    break;
                case "USD":
                    sumUSD += x.calculate();
                    break;
                default:
                    break;
            }
        });
        this.credit.filter((x)=>{
            switch (x.currency) {
                case "UAH":
                    sumEUR += +x.cost;
                    break;
                case "EUR":
                    sumUAH += +x.cost;
                    break;
                case "USD":
                    sumUSD += +x.cost;
                    break;
                default:
                    break;
            }
        });
        return `UAH:${sumEUR.toFixed(2)};EUR:${sumUAH.toFixed(2)};USD:${sumUSD.toFixed(2)};`;  
    }
    getAllOutgo(){
        let sumUAH = 0;
        let sumEUR = 0;
        let sumUSD = 0;
        this.outgo.filter((x)=>{
            switch (x.currency) {
                case "UAH":
                    sumEUR += x.calculate();
                    break;
                case "EUR":
                    sumUAH += x.calculate();
                    break;
                case "USD":
                    sumUSD += x.calculate();
                    break;
                default:
                    break;
            }
        });
        this.contribution.filter((x)=>{
            switch (x.currency) {
                case "UAH":
                    sumEUR += +x.cost;
                    break;
                case "EUR":
                    sumUAH += +x.cost;
                    break;
                case "USD":
                    sumUSD += +x.cost;
                    break;
                default:
                    break;
            }
        });
        return `UAH:${sumEUR.toFixed(2)};EUR:${sumUAH.toFixed(2)};USD:${sumUSD.toFixed(2)};`;  
    }
    getResult(){
        let sumUAH = 0;
        let sumEUR = 0;
        let sumUSD = 0;
        this.income.filter((x)=>{
            switch (x.currency) {
                case "UAH":
                    sumEUR += x.calculate();
                    break;
                case "EUR":
                    sumUAH += x.calculate();
                    break;
                case "USD":
                    sumUSD += x.calculate();
                    break;
                default:
                    break;
            }
        });
        this.credit.filter((x)=>{
            switch (x.currency) {
                case "UAH":
                    sumEUR += +x.cost;
                    break;
                case "EUR":
                    sumUAH += +x.cost;
                    break;
                case "USD":
                    sumUSD += +x.cost;
                    break;
                default:
                    break;
            }
        });
        this.outgo.filter((x)=>{
            switch (x.currency) {
                case "UAH":
                    sumEUR -= x.calculate();
                    break;
                case "EUR":
                    sumUAH -= x.calculate();
                    break;
                case "USD":
                    sumUSD -= x.calculate();
                    break;
                default:
                    break;
            }
        });
        this.contribution.filter((x)=>{
            switch (x.currency) {
                case "UAH":
                    sumEUR -= +x.cost;
                    break;
                case "EUR":
                    sumUAH -= +x.cost;
                    break;
                case "USD":
                    sumUSD -= +x.cost;
                    break;
                default:
                    break;
            }
        });
        return `UAH:${(+this.UAH + sumEUR).toFixed(2)};EUR:${(+this.EUR + sumUAH).toFixed(2)};USD:${(+this.USD + sumUSD).toFixed(2)};`;     
    }
}

class Income{
    constructor(date, category, cost, currency, quantity, note) {
        this.date = date;
        this.category = category;
        this.cost = cost;
        this.currency = currency;
        this.quantity = quantity;
        this.note = note;
    }   
    calculate() {
       return ((+this.cost) * (+this.quantity))
    }
}

class Outgo{
    constructor(date, category, cost, currency, quantity, note) {
        this.date = date;
        this.category = category;
        this.cost = cost;
        this.currency = currency;
        this.quantity = quantity;
        this.note = note;
    }
    calculate() {
        return ((+this.cost) * (+this.quantity))
     }    
}

class Contribution{
    constructor(dateStart, note, duration, durationType, cost, currency, percent) {
        this.dateStart = dateStart;
        this.note = note;
        this.duration = duration;
        this.durationType = durationType;
        this.cost = cost;
        this.currency = currency;
        this.percent = percent;
    }
    getDateEnd() {
        let date = new Date();//
        date.setFullYear(this.dateStart.split("-")[0]); //0-y 1-m 2-d
        date.setMonth(this.dateStart.split("-")[1]-1);//v date may eto 04
        date.setDate(this.dateStart.split("-")[2])
        if (this.durationType == "year") {
            date.setFullYear(date.getFullYear() + +this.duration);
        } else {
            date.setMonth(date.getMonth() + +this.duration);
        }
        return date.toLocaleDateString();
    };
}

class Credit{
    constructor(date, name, cost, currency, percent, duration, durationType) {
        this.date = date;
        this.name = name;
        this.cost = cost;
        this.currency = currency;
        this.percent = percent;
        this.duration = duration;
        this.durationType = durationType;
    }
}

let data = JSON.parse(localStorage.getItem('countsData'));
if (data) { //если он есть

    for (let index = 0; index < data.length; index++) {
        let newCount = new Count();

        newCount.name = data[index].name;
        newCount.color = data[index].color;
        newCount.UAH = data[index].UAH;
        newCount.EUR = data[index].EUR;
        newCount.USD = data[index].USD;
        newCount.note = data[index].note;
        newCount.id = data[index].id;

        newCount.income = [];
        newCount.outgo = [];
        newCount.contribution = [];
        newCount.credit = [];

        for (let i = 0; i < data[index].income.length; i++) {
            let newIncome = new Income();

            newIncome.date = data[index].income[i].date;
            newIncome.category = data[index].income[i].category;
            newIncome.cost = data[index].income[i].cost;
            newIncome.currency = data[index].income[i].currency;
            newIncome.quantity = data[index].income[i].quantity;
            newIncome.note = data[index].income[i].note;

            newCount.income.push(newIncome);            
        }

        for (let i = 0; i < data[index].outgo.length; i++) {
            let newOutgo = new Outgo();

            newOutgo.date = data[index].outgo[i].date;
            newOutgo.category = data[index].outgo[i].category;
            newOutgo.cost = data[index].outgo[i].cost;
            newOutgo.currency = data[index].outgo[i].currency;
            newOutgo.quantity = data[index].outgo[i].quantity;
            newOutgo.note = data[index].outgo[i].note;

            newCount.outgo.push(newOutgo);             
        }

        for (let i = 0; i < data[index].contribution.length; i++) {
            let newСontribution = new Contribution();
            
            newСontribution.dateStart = data[index].contribution[i].dateStart;       
            newСontribution.note = data[index].contribution[i].note;      
            newСontribution.duration = data[index].contribution[i].duration;
            newСontribution.durationType = data[index].contribution[i].durationType;
            newСontribution.cost = data[index].contribution[i].cost;      
            newСontribution.currency = data[index].contribution[i].currency;      
            newСontribution.percent = data[index].contribution[i].percent;      /**/

            newCount.contribution.push(newСontribution);             
        }

        for (let i = 0; i < data[index].credit.length; i++) {
            let newCredit = new Credit();
            
            newCredit.date = data[index].credit[i].date;       
            newCredit.name = data[index].credit[i].name;      
            newCredit.duration = data[index].credit[i].duration;
            newCredit.durationType = data[index].credit[i].durationType;
            newCredit.cost = data[index].credit[i].cost;      
            newCredit.currency = data[index].credit[i].currency;      
            newCredit.percent = data[index].credit[i].percent;      /**/

            newCount.credit.push(newCredit);             
        }

        arrCounts.push(newCount);
        updateNameList(newCount);
        drawOutgoTable();
        drawIncomeTable();
        drawContributionTable();
        drawCreditTable();
    }
}

for (let index = 0; index < document.querySelectorAll(".menu .button").length; index++) {
    document.querySelectorAll(".menu .button")[index].addEventListener("click",(e)=>{
        for (let index = 0; index < document.querySelectorAll(".add-box").length; index++) {
            document.querySelectorAll(".add-box")[index].classList.add("unvisible");//при перекл на друг вкладку все дод карт исчезают
        }
        for (let i = 0; i <  document.querySelectorAll("tr").length; i++) {
            document.querySelectorAll("tr")[i].classList.remove("selected");//при перекл на друг вкладку убир раньше вібр елемент
            selectedElement = "";
        }
        for (let index = 0; index < document.querySelectorAll(".menu .button").length; index++) {
            document.querySelectorAll(".menu .button")[index].classList.remove("selected");
        }
        document.querySelector(".currency-wrapper").classList.add("unvisible");//убир окно с валютой
        e.currentTarget.classList.add("selected");
        if (e.currentTarget.id == "menu-currency") {//если наж на валюту
            showCurrency();//запуск ф-ц котор показ окно с валютой + дел запрос на нбу
        } else{/**/
            showTable(e.currentTarget.id);//показ табл вібраного подменю
        }
    });
    
}
//  
document.getElementById("control-button-add").onclick = function() {
    showAddBox();//візов ф-ц показа карточки добавления
}
/*
addEventListener("click",()=>{//на кнопку добавить вешаем 
    showAddBox();//візов ф-ц показа карточки добавления
});*/

document.getElementById("control-button-change").onclick = function() {
    findSelectedElement('change');//візов ф-ц для нахожд конкретн елем на стр в табл и в масив двнн
}
/*
addEventListener("click",()=>{//на кнопку elfk вешаем 
    findSelectedElement('change');//візов ф-ц для нахожд конкретн елем на стр в табл и в масив двнн
});*/

document.getElementById("control-button-delete").onclick = function() {
    findSelectedElement('delete');//візов ф-ц для нахожд конкретн елем на стр в табл и в масив двнн
}
/*addEventListener("click",()=>{//на кнопку elfk вешаем 
    findSelectedElement('delete');//візов ф-ц для нахожд конкретн елем на стр в табл и в масив двнн
});*/

function showResultField(value) {

    let costs = document.querySelectorAll('input[name="' + value +'-cost"]');
    let quantitys = document.querySelectorAll('input[name="' + value +'-quantity"]');
    let results = document.querySelectorAll('input[name="' + value +'-result"]');
    for (let index = 0; index < costs.length; index++) {
        costs[index].onchange = function () {
            results[index].value = (+costs[index].value * +quantitys[index].value).toFixed(2);
        };
        quantitys[index].onchange = function () {
            results[index].value = (+costs[index].value * +quantitys[index].value).toFixed(2);
        };
    }

}
showResultField("income");
showResultField("outgo");

function showContributionField() {//нужно учесть еще время

    let costs = document.querySelectorAll('input[name="contribution-cost"]');
    let percents = document.querySelectorAll('input[name="contribution-percent"]');
    let results = document.querySelectorAll('input[name="contribution-result"]');

    let durations = document.querySelectorAll('input[name="contribution-duration"]');
    let durationsType = document.querySelectorAll('select[name="contribution-durationType"]'); 

    for (let index = 0; index < costs.length; index++) {
        durations[index].onchange = function () {
            results[index].value = (+costs[index].value * Math.pow((1+(+percents[index].value/100)/12),getTime(durations[index].value,durationsType[index].value))).toFixed(2);
        };
        durationsType[index].onchange = function () {
            results[index].value = (+costs[index].value * Math.pow((1+(+percents[index].value/100)/12),getTime(durations[index].value,durationsType[index].value))).toFixed(2);
        };
        costs[index].onchange = function () {
            results[index].value = (+costs[index].value * Math.pow((1+(+percents[index].value/100)/12),getTime(durations[index].value,durationsType[index].value))).toFixed(2);
        };
        percents[index].onchange = function () {
            results[index].value = (+costs[index].value * Math.pow((1+(+percents[index].value/100)/12),getTime(durations[index].value,durationsType[index].value))).toFixed(2);
        };
    }
    
    //S=P*(1+(l:100):12)^t t - mesaci, if year - t = t*12; l - stavka, p - vklad
    //100000*(1+0.18/12)^3=104567.84
}

function getTime(value,type) {//фнкц расч времени для фрмл депоз
    let time;
    if (type == "year") {
        time = +value * 12;
    } else {
        time = +value;
    }
    return time;
}
showContributionField();
/**/
/*
.addEventListener("change",()=>{ 
    document.querySelector('input[name="income-result"]').value = +document.querySelector('input[name="income-cost"]').value * +document.querySelector('input[name="income-quantity"]').value;
});

onchange = function (params) {
    alert("aaa");
}*/

function findSelectedElement(arg) {
    if (!selectedElement) {
        alert("Оберіть спочатку елемент");
    } else {
        arrCounts.filter((x)=>{
            if (x.id == selectedElement) {
                if (arg == "change") {
                    changeElement(arrCounts.indexOf(x),arrCounts);
                } else{
                    if (confirm("Ви впевненні?")) {
                        deleteElement(arrCounts.indexOf(x),arrCounts);
                    }
                }
            } else {
                x.income.filter((y)=>{
                    if (y.id == selectedElement) {
                        if (arg == "change") {
                            changeElement(x.income.indexOf(y),x.income);
                        } else {            
                            if (confirm("Ви впевненні?")) {
                                deleteElement(x.income.indexOf(y),x.income);
                            }
                        }
                    }
                })
                x.outgo.filter((z)=>{
                    if (z.id == selectedElement) {
                        if (arg == "change") {
                            changeElement(x.outgo.indexOf(z),x.outgo);
                        } else {                                  
                            if (confirm("Ви впевненні?")) {
                                deleteElement(x.outgo.indexOf(z),x.outgo);
                            }
                        }
                    }
                })
                x.contribution.filter((b)=>{
                    if (b.id == selectedElement) {
                        if (arg == "change") {
                            changeElement(x.contribution.indexOf(b),x.contribution);
                        } else {                                 
                            if (confirm("Ви впевненні?")) {
                                deleteElement(x.contribution.indexOf(b),x.contribution);
                            }
                        }
                    }
                })
                x.credit.filter((u)=>{
                    if (u.id == selectedElement) {
                        if (arg == "change") {
                            changeElement(x.credit.indexOf(u),x.credit);
                        } else {                                 
                            if (confirm("Ви впевненні?")) {
                                deleteElement(x.credit.indexOf(u),x.credit);
                            }
                        }
                    }
                })
            } 
        })
    }
}

function changeElement(i,arr) {
    let changedForm;

    //опред какой именно ел вибр и загруз в соотв форму инф с об
    switch (arr[i].constructor) {
        case Count:
            changedForm = document.querySelector("#count-change form");//выбр форму
            document.querySelector("#count-change").classList.remove("unvisible");//тут откл анвизибл

            //загр в форму инф из выбр об  
            changedForm.querySelector('input[name="count-name"]').value = arr[i].name;
            changedForm.querySelector('input[name="count-color"]').value = arr[i].color;
            changedForm.querySelector('input[name="count-UAH"]').value = arr[i].UAH;
            changedForm.querySelector('input[name="count-USD"]').value = arr[i].USD;
            changedForm.querySelector('input[name="count-EUR"]').value = arr[i].EUR;
            changedForm.querySelector('textarea[name="count-note"]').value = arr[i].note;            
            break;

        case Income:
            changedForm = document.querySelector("#income-change form");
            document.querySelector("#income-change").classList.remove("unvisible");//тут откл анвизибл
            
            //загр в форму инф из выбр об  
            changedForm.querySelector('input[name="income-date"]').value = arr[i].date;
            changedForm.querySelector('select[name="income-categories"]').value = arr[i].category;
            changedForm.querySelector('input[name="income-cost"]').value = arr[i].cost;
            changedForm.querySelector('select[name="income-currency"]').value = arr[i].currency;
            changedForm.querySelector('input[name="income-quantity"]').value = arr[i].quantity;
            changedForm.querySelector('input[name="income-result"]').value = +arr[i].cost * +arr[i].quantity;
            changedForm.querySelector('textarea[name="income-note"]').value = arr[i].note;  
            break;  

        case Outgo:
            changedForm = document.querySelector("#outgo-change form");
            document.querySelector("#outgo-change").classList.remove("unvisible");//тут откл анвизибл
                
            //загр в форму инф из выбр об  
            changedForm.querySelector('input[name="outgo-date"]').value = arr[i].date;
            changedForm.querySelector('select[name="outgo-categories"]').value = arr[i].category;
            changedForm.querySelector('input[name="outgo-cost"]').value = arr[i].cost;
            changedForm.querySelector('select[name="outgo-currency"]').value = arr[i].currency;
            changedForm.querySelector('input[name="outgo-quantity"]').value = arr[i].quantity;
            changedForm.querySelector('input[name="outgo-result"]').value = +arr[i].cost * +arr[i].quantity;
            changedForm.querySelector('textarea[name="outgo-note"]').value = arr[i].note;
            break;

        case Contribution:
            changedForm = document.querySelector("#contribution-change form");
            document.querySelector("#contribution-change").classList.remove("unvisible");//тут откл анвизибл
            
            //загр в форму инф из выбр об  
            changedForm.querySelector('input[name="contribution-start"]').value = arr[i].dateStart;
            changedForm.querySelector('input[name="contribution-duration"]').value = arr[i].duration;
            changedForm.querySelector('select[name="contribution-durationType"]').value = arr[i].durationType;
            changedForm.querySelector('input[name="contribution-cost"]').value = arr[i].cost;
            changedForm.querySelector('select[name="contribution-currency"]').value = arr[i].currency;
            changedForm.querySelector('input[name="contribution-percent"]').value = arr[i].percent;
            changedForm.querySelector('textarea[name="contribution-note"]').value = arr[i].note;
            changedForm.querySelector('input[name="contribution-result"]').value = (+arr[i].cost * Math.pow((1+(+arr[i].percent/100)/12),getTime(arr[i].duration,arr[i].durationType))).toFixed(2);   
            break;

        case Credit:
            changedForm = document.querySelector("#credit-change form");
            document.querySelector("#credit-change").classList.remove("unvisible");//тут откл анвизибл
            
            //загр в форму инф из выбр об  
            changedForm.querySelector('input[name="credit-name"]').value = arr[i].name;
            changedForm.querySelector('input[name="credit-start"]').value = arr[i].date;
            changedForm.querySelector('input[name="credit-duration"]').value = arr[i].duration;
            changedForm.querySelector('select[name="credit-durationType"]').value = arr[i].durationType;
            changedForm.querySelector('input[name="credit-cost"]').value = arr[i].cost;
            changedForm.querySelector('select[name="credit-currency"]').value = arr[i].currency;
            changedForm.querySelector('input[name="credit-percent"]').value = arr[i].percent;
            break;
        default:
            break;
    }

    changedForm.querySelector('input[type="submit"]').onclick = function (e) {
        e.preventDefault();//предотвр отправку сабміт с форми
        let changedFormInput = changedForm.querySelectorAll('input');//берем все инпут с форми
        let changedFormSelect = changedForm.querySelectorAll('select');//берем все select с форми
        if (changedForm.querySelector("textarea")) {
            arr[i].note = changedForm.querySelector("textarea").value;//перезап заметки
        }
        
        if (arr[i] instanceof Count) {//если счет сохр нов инфу в вібр счет

            for (let index = 0; index < document.querySelectorAll("#outgo-cart select option").length; index++) {
                if (document.querySelectorAll("#outgo-cart select option")[index].name == arr[i].name) {
                    document.querySelectorAll("#outgo-cart select option")[index].remove();
                    //
                }
            }
            for (let index = 0; index < document.querySelectorAll("#contribution-cart select option").length; index++) {
                if (document.querySelectorAll("#contribution-cart select option")[index].name == arr[i].name) {
                    document.querySelectorAll("#contribution-cart select option")[index].remove();
                    //
                }
            }
            for (let index = 0; index < document.querySelectorAll("#income-cart select option").length; index++) {
                if (document.querySelectorAll("#income-cart select option")[index].name == arr[i].name) {
                    document.querySelectorAll("#income-cart select option")[index].remove();
                    //
                    //document.querySelectorAll("#income-cart select option")[index].re
                }
            }

            arr[i].name = changedFormInput[0].value;
            arr[i].color = changedFormInput[1].value;
            arr[i].UAH = changedFormInput[2].value;
            arr[i].USD = changedFormInput[3].value;
            arr[i].EUR = changedFormInput[4].value;       
            updateNameList(arr[i]);    
        } else {
            if (arr[i] instanceof Contribution) {
                arr[i].dateStart = changedFormInput[0].value;
                arr[i].duration = changedFormInput[1].value;
                arr[i].durationType = changedFormSelect[0].value;
                arr[i].cost = changedFormInput[2].value;
                arr[i].currency = changedFormSelect[1].value;
                arr[i].percent = changedFormInput[3].value;
                
                
            } else {
                if (arr[i] instanceof Credit) {
                    arr[i].name = changedFormInput[0].value;
                    arr[i].date = changedFormInput[1].value;
                    arr[i].duration = changedFormInput[2].value;
                    arr[i].durationType = changedFormSelect[0].value;
                    arr[i].cost = changedFormInput[3].value;
                    arr[i].currency = changedFormSelect[1].value;
                    arr[i].percent = changedFormInput[4].value;                    
                } else {
                    //иначе либо доход либо расход. зап инфу туда
                    arr[i].date = changedFormInput[0].value;
                    arr[i].category = changedFormSelect[0].value;
                    arr[i].cost = changedFormInput[1].value;
                    arr[i].currency = changedFormSelect[1].value;
                    arr[i].quantity = changedFormInput[2].value; /* */                     
                }
            }
        }
        drawIncomeTable();//перерис табл
        drawOutgoTable();
        drawContributionTable();//перерис табл
        drawCreditTable();
        changedForm.parentElement.classList.add("unvisible");
    };
    changedForm.querySelector('input[type="reset"]').onclick = function (e) {
        changedForm.parentElement.classList.add("unvisible");
    };
}

function deleteElement(i,arr) {
    if (arr == arrCounts) {//если удал счет

        let deletedName = arr[i].name //указ имя удал счет
        let nameListIncome = document.querySelectorAll("#income-cart select option");//вибир список счетов с дохода
        let nameListOutgo = document.querySelectorAll("#outgo-cart select option");//вибир список счетов с расхода
        
        for (let index = 0; index < nameListIncome.length; index++) {//в цикл исчем удал имя и убираем из списка
            if (nameListIncome[index].name == deletedName) {
                nameListIncome[index].remove();
            }
        }
        for (let index = 0; index < nameListOutgo.length; index++) {//в цикл исчем удал имя и убираем из списка
            if (nameListOutgo[index].name == deletedName) {
                nameListOutgo[index].remove();
            }
        }

        arr.splice(i, 1);//удал елем из масива
        selectedElement = "";
        for (let index = 0; index < arrCounts.length; index++) {
           arrCounts[index].id = index;  //еще надо допис обновл ид в самом масиве счетов
        }
        drawCountTable();//перерис осн табл для обновл ид на табл счетов на стр
        location.reload();  //перезагр стр для обновл табл дох и расх, cred & count   

    } else {
        arr.splice(i, 1);//удал елем из масива
        document.getElementById(selectedElement).remove();//удал елем из табл на стр
        selectedElement = "";
    }
    drawIncomeTable();//перерис табл
    drawOutgoTable();//перерис табл
    drawContributionTable();
    drawCreditTable();
}

function showCurrency() {
    let currencyInputs = document.querySelectorAll(".input-currency");
    getDataNBY();
    for (let index = 0; index < currencyInputs.length; index++) {
        currencyInputs[index].addEventListener("input",(e)=>{
            let inputNumber = e.currentTarget.value;//введен число string
            let inputNumberType = e.currentTarget.id;
            if (inputNumber.match(/^\d{1,}(.\d{0,2})?$/)) { //пров через регвир на соответств
                calculateCurrency(inputNumber,inputNumberType)
            } else {
                alert("Тільки цифри з максимум 2 знаками після коми");
            }
        });
    }
    let currencyOutputs= document.querySelectorAll(".result-wrapper span");
    document.getElementById("currency-box-button").addEventListener("click",()=>{
        document.querySelector(".currency-wrapper").classList.add("unvisible");
        document.querySelector(".text").innerText = "";
        for (let index = 0; index < currencyInputs.length; index++) {
            currencyInputs[index].value = "0.00";
            currencyOutputs[index].innerText = "0";
        }
    })
}

function calculateCurrency(number,type) {
    let result;
    let resultType ="result" + type;
    switch (type) {
        case "UAHtoUSD": 
            result = +number/rateNBY[0].rate;
            break;
        case "USDtoUAH":
            result = +number*rateNBY[0].rate;
            break;
        case "UAHtoEUR":
            result = +number/rateNBY[1].rate;
            break;
        case "EURtoUAH":
            result = +number*rateNBY[1].rate;
            break;    
        default:
            break;
    }
    document.getElementById(resultType).innerText = result.toFixed(2);
}

function createCount(data) { //функц созд нового счета
    let formData = new FormData(data)//инф с формі
    let countName = formData.get("count-name"); //имя с формі

    function testName(name) {//фнкц для нахожд пуст или одинаков названий
        let answer;
        if (countName == "") {
            answer = true;
        } else {
            arrCounts.filter((x)=>{//ищем рахунок с такой же назв
                if (x.name == name) {
                    answer = true;
                } 
            })  
        }
        return answer;
    }

    if (testName(countName)) {
        alert("Треба написати унікальну назву");
    } else {
        let newCount = new Count();//если назв уникальн то созд нов рахунок
        //заполн его инф с формі
        newCount.name = countName;
        newCount.color = formData.get("count-color");
        newCount.note = formData.get("count-note");
        newCount.EUR = formData.get("count-EUR")
        newCount.UAH = formData.get("count-UAH")
        newCount.USD = formData.get("count-USD");
        newCount.income = [];//масив для доходов
        newCount.outgo = [];//масив для расходов
        newCount.contribution = [];//масив для вкладов
        newCount.credit = [];//масив для долгов

        arrCounts.push(newCount);//сохр созд рахунок в масив рахунків
        newCount.id = arrCounts.length;//получ ід созд рахунка
        //созд дополн опции вибора счета для дох и расх
        updateNameList(newCount);
        drawCountTable();      
    }
}

function updateNameList(newName) {
    let optInc = document.createElement("option");
    let optOut = document.createElement("option");
    let optCont = document.createElement("option");
    let optCred = document.createElement("option");
    optInc.name = newName.name;
    optInc.innerText = newName.name;
    optOut.name = newName.name;
    optOut.innerText = newName.name;
    optCont.name = newName.name;
    optCont.innerText = newName.name;
    optCred.name = newName.name;
    optCred.innerText = newName.name;
    document.querySelector("#income-cart select").appendChild(optInc);
    document.querySelector("#outgo-cart select").appendChild(optOut);    
    document.querySelector("#contribution-cart select").appendChild(optCont);   
    document.querySelector("#credit-cart select").appendChild(optCred); 
}

function createIncome(data) {
    let formData = new FormData(data);
    let newIncome = new Income();
    let selectedCount = formData.get("count");
    if (selectedCount == "none") {
        alert("Треба вибрати вже існуючий рахунок або спочатку створити новий");
    } else {
        newIncome.date = formData.get("income-date");
        newIncome.category = formData.get("income-categories");
        newIncome.cost = formData.get("income-cost")
        newIncome.currency = formData.get("income-currency")
        newIncome.quantity = formData.get("income-quantity")
        newIncome.note = formData.get("income-note");
        arrCounts.filter((x)=>{
            if (x.name == selectedCount) {
                x.income.push(newIncome);
            }
        })
        drawIncomeTable();
    }
}

function createOutgo(data) {
    let formData = new FormData(data);
    let newOutgo = new Outgo();
    let selectedCount = formData.get("count");
    if (selectedCount == "none") {
        alert("Треба вибрати вже існуючий рахунок або спочатку створити новий");
    } else {
        newOutgo.date = formData.get("outgo-date");
        newOutgo.category = formData.get("outgo-categories");
        newOutgo.cost = formData.get("outgo-cost")
        newOutgo.currency = formData.get("outgo-currency")
        newOutgo.quantity = formData.get("outgo-quantity")
        newOutgo.note = formData.get("outgo-note");
        arrCounts.filter((x)=>{
            if (x.name == selectedCount) {
                x.outgo.push(newOutgo);
            }
        });
        drawOutgoTable();
    }
}

function createContribution(data) {
    let formData = new FormData(data);
    let newContribution = new Contribution();
    let selectedCount = formData.get("count");
    if (selectedCount == "none") {
        alert("Треба вибрати вже існуючий рахунок або спочатку створити новий");
    } else {
        newContribution.dateStart = formData.get("contribution-start");
        if (newContribution.dateStart) {
            newContribution.note = formData.get("contribution-note");
            newContribution.duration = formData.get("contribution-duration");
            newContribution.durationType = formData.get("contribution-durationType");
            newContribution.percent = formData.get("contribution-percent");
            newContribution.cost = formData.get("contribution-cost");
            newContribution.currency = formData.get("contribution-currency");
        
            arrCounts.filter((x)=>{
                if (x.name == selectedCount) {
                    x.contribution.push(newContribution);
                }
            });

        } else {
            alert("Треба вибрати початкову дату");
        }
        drawContributionTable();
    }
}

function createCredit(data) {//dodel funct + dod v count nov masiv dla contribution
    let formData = new FormData(data);
    let newCredit = new Credit();
    let selectedCount = formData.get("count");
    if (selectedCount == "none") {
        alert("Треба вибрати вже існуючий рахунок або спочатку створити новий");
    } else {
        newCredit.date = formData.get("credit-start");
        if (newCredit.date) {
            newCredit.name = formData.get("credit-name");
            newCredit.cost = formData.get("credit-cost");
            newCredit.currency = formData.get("credit-currency");
            newCredit.duration = formData.get("credit-duration");
            newCredit.durationType = formData.get("credit-durationType");
            newCredit.percent = formData.get("credit-percent");
        
            arrCounts.filter((x)=>{
                if (x.name == selectedCount) {
                    x.credit.push(newCredit);
                }
            }) 
        } else {
            alert("Треба вибрати початкову дату");
        }
        drawCreditTable();
    }
}

function getForm(string) {//фнкц для раб с формой(карточки добавл)
    let selected = string + " form";
    let selectForm = document.querySelector(selected);

    function resetForm() {//убир собітия с формі чтобі не накапливались
        document.querySelector(string).classList.add("unvisible");
        selectForm.onsubmit = null;
        selectForm.onreset = null; 
        //selectForm.removeEventListener('reset',resetForm);          //selectForm.onsubmit = null;
        //selectForm.removeEventListener('submit',choseObject);   //selectForm.onreset = null; 
    }

    function choseObject(event) {//вібір какой именно об созд
        event.preventDefault();
        switch (string) {
            case "#count-cart":
                createCount(selectForm);
                break;
            case "#income-cart":
                createIncome(selectForm);
                break;
            case "#outgo-cart":
                createOutgo(selectForm);
                break;     
            case "#contribution-cart":
                createContribution(selectForm);
                break;    
            case "#credit-cart":
                createCredit(selectForm);
                break;  
            default:
                break;
        }
          //eventList zamen na onEvent   ybr sobit onclick = null
        document.querySelector(string).classList.add("unvisible");
        selectForm.onsubmit = null;
        selectForm.onreset = null;
        //selectForm.removeEventListener('submit',choseObject);  //selectForm.onsubmit = null;
        //selectForm.removeEventListener('reset',resetForm);     //selectForm.onreset = null;  
    }
    selectForm.onsubmit = choseObject;    
    selectForm.onreset = resetForm;
    //selectForm.addEventListener('submit',choseObject);  //selectForm.onsubmit = choseObject;
    //selectForm.addEventListener("reset",resetForm);     //selectForm.onreset = resetForm;
}

function showAddBox() {
    switch (selectedMenu) {
        case "menu-count":
            document.querySelector("#count-cart").classList.remove("unvisible");
            getForm("#count-cart");
            break;
        case "menu-income":
            document.querySelector("#income-cart").classList.remove("unvisible");
            getForm("#income-cart");
            break;
        case "menu-outgo":
            document.querySelector("#outgo-cart").classList.remove("unvisible");
            getForm("#outgo-cart");
            break;
        case "menu-credit":
            document.querySelector("#credit-cart").classList.remove("unvisible");
            getForm("#credit-cart");
            break;
        case "menu-contributions":
            document.querySelector("#contribution-cart").classList.remove("unvisible");
            getForm("#contribution-cart");
            break;    
        default:
            break;
    }
}

function getDataNBY() { // получ масив валют с нбу
    const dataNBY = fetch("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");
    dataNBY.then((info)=>{
        return info.json();
    }).then((info)=>{
        showData(info);
    }).catch((err)=>{
        alert("Виникла помилка із сервером");
        console.error(err);
    })
}

function showData(info) {
    const text = document.querySelector(".text");
    info.filter((e)=>{//шук дол та евр
        if (e.cc == "USD" || e.cc == "EUR") {
            return e;
        } 
    }).forEach(e => {
        text.insertAdjacentText("beforeend",` ${e.cc} = ${e.rate}.`);
        rateNBY.push(e);
    });
    document.querySelector(".currency-wrapper").classList.remove("unvisible");
    
}

function addVoidRow(type) {// ф-нкц для додав пуст строк чтоб при удал ел-тов таблица не седалась 
    let captionsRow = '.' + type + '-table thead tr td';
    let tableSelector = '.' + type + '-table tbody tr';
    let captions = document.querySelectorAll(captionsRow)
    let table = document.querySelectorAll(tableSelector);
    //document.querySelectorAll(".outgo-table thead tr td")
    //24
    while (table.length < 23) {
        let newTableRow = document.createElement("tr");
        while (newTableRow.children.length < captions.length) {
            let newTableCell = document.createElement("td");
            newTableCell.innerText = ' ';
            newTableRow.appendChild(newTableCell);
        }
        table[1].parentElement.appendChild(newTableRow);
        table = document.querySelectorAll(tableSelector);
        //перепис табле
    }
}/**/

function renameEng(table, type) {

    let categoriesName = '[name="'+type + '-categories"] option';
    //let names = document.querySelectorAll(categoriesName+1)
    let names = document.querySelectorAll(categoriesName);
    //names[1].innerText  names[1].value

    table.forEach((x)=>{
        x.querySelectorAll("td").forEach((y)=>{
            for (let index = 0; index < names.length; index++) {
                if (y.innerText == names[index].value) {
                    y.innerText = names[index].innerText;
                }
            }
        });
    });
}

function showTable(select){// надо нап внутр фнкц для чтобі прятал все табл
    selectedMenu = select;
    function hideTables() {
        document.querySelector(".count-table").classList.add("unvisible");
        document.querySelector(".outgo-table").classList.add("unvisible");
        document.querySelector(".income-table").classList.add("unvisible");
        document.querySelector(".contribution-table").classList.add("unvisible");  
        document.querySelector(".credit-table").classList.add("unvisible");  
        
    }
    switch (select) {
        case "menu-count":
            hideTables();
            document.querySelector(".count-table").classList.remove("unvisible");
            break;
        case "menu-outgo":
            hideTables();
            document.querySelector(".outgo-table").classList.remove("unvisible");
            break;
        case "menu-income":
            hideTables();
            document.querySelector(".income-table").classList.remove("unvisible");
            break;
         case "menu-credit":
            hideTables();
            document.querySelector(".credit-table").classList.remove("unvisible");
            break;         
        case "menu-contributions":
            hideTables();
            document.querySelector(".contribution-table").classList.remove("unvisible");
            break;     
        default:
            break;
    }
}

function drawCountTable() {
    let tableCount = document.querySelectorAll(".count-table tbody tr");
    let clearTableCount = [];//масив для пустіх строк
    tableCount.forEach((x)=>{if(!x.id){clearTableCount.push(x)}});
    while (arrCounts.length > clearTableCount.length) {// если кол-во пуст строк меньше кол-ва доходов
        let newTableRow = document.createElement("tr");
        document.querySelector(".count-table tbody").appendChild(newTableRow); //созд и добав нов строки
        clearTableCount.push(newTableRow);   //добав созд строку в масив пуст строк
        tableCount = document.querySelectorAll(".count-table tbody tr"); // снова собир псевдомасив всех строк из таблиц доходов         
    }

    for (let index = 0; index < arrCounts.length; index++) {
        tableCount[index].innerHTML = "";
        tableCount[index].insertAdjacentHTML("beforeend",`                
            <td class="">${index+1}</td>
            <td class="">${arrCounts[index].name}</td>
            <td class="">${arrCounts[index].getAllOutgo()}</td>
            <td class="">${arrCounts[index].getAllIncome()}</td>
            <td class="">${arrCounts[index].getResult()}</td>
            <td class="">${arrCounts[index].note}</td>
        `);
        tableCount[index].style.backgroundColor = arrCounts[index].color;
        tableCount[index].id = arrCounts[index].id;
        

        tableCount[index].onclick = function (e) {//вешаем собітие клик на каждую строку табл через онклік чтобі соб срабатівало только 1 раз тк евент лист через постоянніе перезаписи накапливается
           
            for (let j = 0; j < tableCount.length; j++) {
                tableCount[j].classList.remove("selected");
            }

            e.target.parentNode.classList.add("selected");
            selectedElement = e.target.parentNode.id;
        }
    }
    
    localStorage.setItem('countsData', JSON.stringify(arrCounts));
    clearTableCount.length = 0;//очищ масив пуст елементов т.к. после вып прогр они уже не пуст
    tableCount.forEach((x)=>{if(!x.id && !x.hasChildNodes()){x.remove()}});//убир лишн пуст строки, если есть
    addVoidRow("count");//для естетики на табл должно всегда біть 23 пуст строки
}

function drawIncomeTable() {
    let tableIncome = document.querySelectorAll(".income-table tbody tr");//псевдомасив всех строк из таблиц доходов
    let clearTableIncome = [];//масив для пустіх строк
    
    let a = 0;//счетчик для ид
    for (let indexCounts = 0; indexCounts < arrCounts.length; indexCounts++) {//перебираем счета
        tableIncome.forEach((x)=>{if(!x.id){clearTableIncome.push(x)}});//собираем общ кол-во пуст строк

        for (let indexIncome = 0; indexIncome < arrCounts[indexCounts].income.length; indexIncome++) {//перебир доході текущ счета
            
            while (arrCounts[indexCounts].income.length > clearTableIncome.length) {// если кол-во пуст строк меньше кол-ва доходов
                let newTableRow = document.createElement("tr");
                document.querySelector(".income-table tbody").appendChild(newTableRow); //созд и добав нов строки
                clearTableIncome.push(newTableRow);   //добав созд строку в масив пуст строк
                tableIncome = document.querySelectorAll(".income-table tbody tr"); // снова собир псевдомасив всех строк из таблиц доходов         
            }

            tableIncome[a].innerHTML = "";
            tableIncome[a].insertAdjacentHTML("beforeend",`                
                <td class="">${arrCounts[indexCounts].income[indexIncome].date}</td>
                <td class="">${arrCounts[indexCounts].name}</td>
                <td class="">${arrCounts[indexCounts].income[indexIncome].category}</td>
                <td class="">${arrCounts[indexCounts].income[indexIncome].quantity}</td>
                <td class="">${arrCounts[indexCounts].income[indexIncome].currency}:${arrCounts[indexCounts].income[indexIncome].cost}</td>
                <td class="">${arrCounts[indexCounts].income[indexIncome].currency}:${arrCounts[indexCounts].income[indexIncome].calculate()}</td>
                <td class="">${arrCounts[indexCounts].income[indexIncome].note}</td>
                `);
            tableIncome[a].id = "income"+a;
            arrCounts[indexCounts].income[indexIncome].id = "income"+a;
            tableIncome[a].style.backgroundColor = arrCounts[indexCounts].color;
            tableIncome[a].onclick = function (e) {
                //alert(e.target.parentNode.id);
                for (let j = 0; j < tableIncome.length; j++) {
                    tableIncome[j].classList.remove("selected");
                }
                e.target.parentNode.classList.add("selected");
                selectedElement = e.target.parentNode.id;
            }
            //tableIncome[a].insertAdjacentElement("afterEnd",newTableRow)
            a++;            
        }
        clearTableIncome.length = 0;//очищ масив пуст елементов т.к. после вып прогр они уже не пуст
    }
    renameEng(tableIncome, "income");
    drawCountTable();//перерис осн табл
    tableIncome.forEach((x)=>{if(!x.id && !x.hasChildNodes()){x.remove()}});//убир лишн пуст строки, если есть
    addVoidRow("income");//для естетики на табл должно всегда біть 23 пуст строки
}

function drawOutgoTable() {
    let tableOutgo = document.querySelectorAll(".outgo-table tbody tr");
    let clearTableOutgo = [];//масив для пустіх строк

    let a = 0;
    for (let indexCounts = 0; indexCounts < arrCounts.length; indexCounts++) {
        tableOutgo.forEach((x)=>{if(!x.id){clearTableOutgo.push(x)}});//собираем общ кол-во пуст строк

        for (let indexOutgo = 0; indexOutgo < arrCounts[indexCounts].outgo.length; indexOutgo++) {

            while (arrCounts[indexCounts].outgo.length > clearTableOutgo.length) {// если кол-во пуст строк меньше кол-ва доходов
                let newTableRow = document.createElement("tr");
                document.querySelector(".outgo-table tbody").appendChild(newTableRow); //созд и добав нов строки
                clearTableOutgo.push(newTableRow);   //добав созд строку в масив пуст строк
                tableOutgo = document.querySelectorAll(".outgo-table tbody tr"); // снова собир псевдомасив всех строк из таблиц доходов         
            }

            tableOutgo[a].innerHTML = "";
            tableOutgo[a].insertAdjacentHTML("beforeend",`                
                <td class="">${arrCounts[indexCounts].outgo[indexOutgo].date}</td>
                <td class="">${arrCounts[indexCounts].name}</td>
                <td class="">${arrCounts[indexCounts].outgo[indexOutgo].category}</td>
                <td class="">${arrCounts[indexCounts].outgo[indexOutgo].quantity}</td>
                <td class="">${arrCounts[indexCounts].outgo[indexOutgo].currency}:${arrCounts[indexCounts].outgo[indexOutgo].cost}</td>
                <td class="">${arrCounts[indexCounts].outgo[indexOutgo].currency}:${arrCounts[indexCounts].outgo[indexOutgo].calculate()}</td>
                <td class="">${arrCounts[indexCounts].outgo[indexOutgo].note}</td>
                `);
            tableOutgo[a].id = "outgo"+a;
            arrCounts[indexCounts].outgo[indexOutgo].id = "outgo"+a;
            tableOutgo[a].style.backgroundColor = arrCounts[indexCounts].color;
            tableOutgo[a].onclick = function (e) {

                for (let j = 0; j < tableOutgo.length; j++) {
                    tableOutgo[j].classList.remove("selected");
                }
                e.target.parentNode.classList.add("selected");
                selectedElement = e.target.parentNode.id;
            }
            a++;              
        }
        clearTableOutgo.length = 0;//очищ масив пуст елементов т.к. после вып прогр они уже не пуст
    }
    renameEng(tableOutgo, "outgo");
    drawCountTable();
    tableOutgo.forEach((x)=>{if(!x.id && !x.hasChildNodes()){x.remove()}});//убир лишн пуст строки, если есть
    addVoidRow("outgo");//для естетики на табл должно всегда біть 23 пуст строки
}

function  drawContributionTable() {
    let tableContribution = document.querySelectorAll(".contribution-table tbody tr");
    let clearTableContribution = [];//масив для пустіх строк

    let a = 0;
    for (let indexCounts = 0; indexCounts < arrCounts.length; indexCounts++) {
        clearTableContribution.forEach((x)=>{if(!x.id){clearTableContribution.push(x)}});//собираем общ кол-во пуст строк

        for (let indexContribution = 0; indexContribution < arrCounts[indexCounts].contribution.length; indexContribution++) {

            while (arrCounts[indexCounts].contribution.length > clearTableContribution.length) {// если кол-во пуст строк меньше кол-ва contribution
                let newTableRow = document.createElement("tr");
                document.querySelector(".contribution-table tbody").appendChild(newTableRow); //созд и добав нов строки
                clearTableContribution.push(newTableRow);   //добав созд строку в масив пуст строк
                tableContribution = document.querySelectorAll(".contribution-table tbody tr"); // снова собир псевдомасив всех строк из таблиц contribution         
            }/*
            let time = +arrCounts[indexCounts].contribution[indexContribution].duration;
            //let p = 1;
            if (arrCounts[indexCounts].contribution[indexContribution].durationType == "year") {
                time = time * 12;
            }*/
            
            let pay = +arrCounts[indexCounts].contribution[indexContribution].cost * Math.pow((1+(+arrCounts[indexCounts].contribution[indexContribution].percent/100)/12),getTime(arrCounts[indexCounts].contribution[indexContribution].duration,arrCounts[indexCounts].contribution[indexContribution].durationType));
            //S=P*(1+(l:100):12)^t t - mesaci, if year - t = t*12; l - stavka, p - vklad
            //100000*(1+0.18/12)^3=104567.84
            
            tableContribution[a].innerHTML = "";
            tableContribution[a].insertAdjacentHTML("beforeend",`    
                <td class="">${arrCounts[indexCounts].name}</td>    
                <td class="">${arrCounts[indexCounts].contribution[indexContribution].currency}: ${arrCounts[indexCounts].contribution[indexContribution].cost}</td>
                <td class="">${arrCounts[indexCounts].contribution[indexContribution].percent}</td>  
                <td class="">${arrCounts[indexCounts].contribution[indexContribution].duration} ${arrCounts[indexCounts].contribution[indexContribution].durationType}</td>   
                <td class="">${arrCounts[indexCounts].contribution[indexContribution].dateStart}</td>
                <td class="">${arrCounts[indexCounts].contribution[indexContribution].getDateEnd()}</td>
                <td class="">${arrCounts[indexCounts].contribution[indexContribution].currency}: ${pay.toFixed(2)}</td>
                <td class="">${arrCounts[indexCounts].contribution[indexContribution].note}</td>
                `);
            tableContribution[a].id = "contribution"+a;
            arrCounts[indexCounts].contribution[indexContribution].id = "contribution"+a;
            tableContribution[a].style.backgroundColor = arrCounts[indexCounts].color;
            tableContribution[a].onclick = function (e) {
                for (let j = 0; j < tableContribution.length; j++) {
                    tableContribution[j].classList.remove("selected");
                }
                e.target.parentNode.classList.add("selected");
                selectedElement = e.target.parentNode.id;
            }
            a++;              
        }
        clearTableContribution.length = 0;//очищ масив пуст елементов т.к. после вып прогр они уже не пуст
    }
    drawCountTable();
    tableContribution.forEach((x)=>{if(!x.id && !x.hasChildNodes()){x.remove()}});//убир лишн пуст строки, если есть
    addVoidRow("contribution");//для естетики на табл должно всегда біть 23 пуст строки
}

function  drawCreditTable() {
    let tableCredit = document.querySelectorAll(".credit-table tbody tr");
    let clearTableCredit = [];//масив для пустіх строк

    let a = 0;
    for (let indexCounts = 0; indexCounts < arrCounts.length; indexCounts++) {
        clearTableCredit.forEach((x)=>{if(!x.id){clearTableCredit.push(x)}});//собираем общ кол-во пуст строк

        for (let indexCredit = 0; indexCredit < arrCounts[indexCounts].credit.length; indexCredit++) {

            while (arrCounts[indexCounts].credit.length > clearTableCredit.length) {// если кол-во пуст строк меньше кол-ва credit
                let newTableRow = document.createElement("tr");
                document.querySelector(".credit-table tbody").appendChild(newTableRow); //созд и добав нов строки
                clearTableCredit.push(newTableRow);   //добав созд строку в масив пуст строк
                tableCredit = document.querySelectorAll(".credit-table tbody tr"); // снова собир псевдомасив всех строк из таблиц contribution         
            }
            //let time = +arrCounts[indexCounts].credit[indexCredit].duration;//dlitelnost
            let p = (+arrCounts[indexCounts].credit[indexCredit].percent/100)/12; //proc v 1 mesac
            /*if (arrCounts[indexCounts].credit[indexCredit].durationType == "year") {//esli god perevod v mesaci
                time = time*12;
            }*/
            let creditTime = getTime(arrCounts[indexCounts].credit[indexCredit].duration,arrCounts[indexCounts].credit[indexCredit].durationType)
            let pay;
            if (+arrCounts[indexCounts].credit[indexCredit].percent == 0) {
                pay = +arrCounts[indexCounts].credit[indexCredit].cost/creditTime;
            } else {
                pay = +arrCounts[indexCounts].credit[indexCredit].cost * (p + (p/(Math.pow((1+p),creditTime) -1)));
            }
            //v = S * (p + p/((1+p)^n-1) p = l/100/12, l - stavka, n - mesaci if year - n = n*12; S - credit, v - ejemes platez
            //400000*((12/100/12)+((12/100/12)/((1+(12/100/12))^24-1))) = 18829 splata v misac

            tableCredit[a].innerHTML = "";
            tableCredit[a].insertAdjacentHTML("beforeend",`   

                <td class="">${arrCounts[indexCounts].credit[indexCredit].date}</td>
                <td class="">${arrCounts[indexCounts].name}</td>    
                <td class="">${arrCounts[indexCounts].credit[indexCredit].name}</td>
                <td class="">${arrCounts[indexCounts].credit[indexCredit].currency}: ${arrCounts[indexCounts].credit[indexCredit].cost}</td>
                <td class="">${arrCounts[indexCounts].credit[indexCredit].percent}</td>  
                <td class="">${arrCounts[indexCounts].credit[indexCredit].duration} ${arrCounts[indexCounts].credit[indexCredit].durationType}</td>   
                
                <td class="">${arrCounts[indexCounts].credit[indexCredit].currency}: ${pay.toFixed(2)}</td>
                <td class="">${arrCounts[indexCounts].credit[indexCredit].currency}: ${(pay*creditTime).toFixed(2)}</td>
                
                `);
            tableCredit[a].id = "credit"+a;
            arrCounts[indexCounts].credit[indexCredit].id = "credit"+a;
            tableCredit[a].style.backgroundColor = arrCounts[indexCounts].color;
            tableCredit[a].onclick = function (e) {

                for (let j = 0; j < tableCredit.length; j++) {
                    tableCredit[j].classList.remove("selected");
                }
                e.target.parentNode.classList.add("selected");
                selectedElement = e.target.parentNode.id;

            }
            a++;              
        }
        clearTableCredit.length = 0;//очищ масив пуст елементов т.к. после вып прогр они уже не пуст
    }
    drawCountTable();
    tableCredit.forEach((x)=>{if(!x.id && !x.hasChildNodes()){x.remove()}});//убир лишн пуст строки, если есть
    addVoidRow("credit");//для естетики на табл должно всегда біть 23 пуст строки
}


