/*
Реалізуйте програму перевірки телефону
* Використовуючи JS Створіть поле для введення телефону та кнопку збереження
* Користувач повинен ввести номер телефону у форматі 000-000-00-00
* Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, відобразіть її в діві до input.


Зображення для відображення
https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
!!https://naukatv.ru/upload/files/shutterstock_418733752.jpg
https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg
https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg
    */


window.addEventListener("load", () => {

/* 
    Створіть програму секундомір.
    * Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"<br>
    * При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий
    * Виведення лічильників у форматі ЧЧ:ММ:СС<br>
    * Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/

    const [...numbers] = document.querySelectorAll("span");
    const [...buttons] = document.querySelectorAll("button");
    
    buttons[2].addEventListener("click", () => { //resrt
    
        document.querySelector(".container-stopwatch").classList.remove("red");
        document.querySelector(".container-stopwatch").classList.remove("black");
        document.querySelector(".container-stopwatch").classList.remove("green");
        document.querySelector(".container-stopwatch").classList.add("silver");
    
        for (let elem in time) {
            time[elem] = 0;
        }
    
        flag = false;
        numbers.forEach((el)=>{
            el.innerText = "0"
        })
    })
    
    buttons[1].addEventListener("click", () => { //stop
        document.querySelector(".container-stopwatch").classList.add("red");
        document.querySelector(".container-stopwatch").classList.remove("black");
        document.querySelector(".container-stopwatch").classList.remove("green");
        document.querySelector(".container-stopwatch").classList.remove("silver");
        clearInterval(timerHandler);
        flag = false;
    })
    
    buttons[0].addEventListener("click", () => { //start
    
        document.querySelector(".container-stopwatch").classList.remove("red");
        document.querySelector(".container-stopwatch").classList.remove("black");
        document.querySelector(".container-stopwatch").classList.add("green");
        document.querySelector(".container-stopwatch").classList.remove("silver");
    
        if (flag) {
        } else {
            timerHandler = setInterval(timer, 1000);
            flag = true;
        } 
    })
    let time = {
        seconds : 0,
        tenSeconds : 0,
        minutes : 0,
        tenMinutes : 0,
        hours : 0,
        tenHours : 0,
    }
    
    let timerHandler;
    let flag = false;
    let timer = () => {
    
        time.seconds++;
        numbers[5].innerText = time.seconds;
    
            if (time.seconds == 10) {
                time.seconds = 0;
                numbers[5].innerText = time.seconds;
                time.tenSeconds++;
                numbers[4].innerText = time.tenSeconds;
    
                if (time.tenSeconds == 6) {
                    time.seconds = 0;
                    numbers[5].innerText = time.seconds;
                    time.tenSeconds = 0;
                    numbers[4].innerText = time.tenSeconds;
                    time.minutes++;
                    numbers[3].innerText = time.minutes;
    
                    if (time.minutes == 10) {
                        time.seconds = 0;
                        numbers[5].innerText = time.seconds;
                        time.tenSeconds = 0;
                        numbers[4].innerText = time.tenSeconds;
                        time.minutes = 0;
                        numbers[3].innerText = time.minutes;
                        time.tenMinutes++;
                        numbers[2].innerText = time.tenMinutes;
    
                        if (time.tenMinutes == 6) {
                            time.seconds = 0;
                            numbers[5].innerText = time.seconds;
                            time.tenSeconds = 0;
                            numbers[4].innerText = time.tenSeconds;
                            time.minutes = 0;
                            numbers[3].innerText = time.minutes;
                            time.tenMinutes = 0;
                            numbers[2].innerText = time.tenMinutes;
                            time.hours++;
                            numbers[1].innerText = time.hours;
    
                            if (time.hours == 10) {
                                time.seconds = 0;
                                numbers[5].innerText = time.seconds;
                                time.tenSeconds = 0;
                                numbers[4].innerText = time.tenSeconds;
                                time.minutes = 0;
                                numbers[3].innerText = time.minutes;
                                time.tenMinutes = 0;
                                numbers[2].innerText = time.tenMinutes;
                                time.hours = 0;
                                numbers[1].innerText = time.hours;   
                                time.tenHours++;  
                                numbers[0].innerText = time.tenHours;                      
                            }
                        }
                    }
                }
    
                
            }
    } 
    
        /*
Реалізуйте програму перевірки телефону
* Використовуючи JS Створіть поле для введення телефону та кнопку збереження
* Користувач повинен ввести номер телефону у форматі 000-000-00-00
* Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, відобразіть її в діві до input.
*/
  
    document.querySelectorAll("h2")[1].after(document.createElement("form"));
    document.querySelector("form").append((document.createElement("input")),(document.createElement("input")));

    const [...inputs] = document.querySelectorAll("input");
    let pattern = /\d{3}-\d{3}-\d{2}-\d{2}$/gi;

    inputs[0].setAttribute('placeholder','000-000-00-00');
    inputs[1].setAttribute('type','button');
    inputs[1].setAttribute('value','Зберегти');
    inputs[1].classList.add("save-button");

    inputs[1].addEventListener("click",()=>{
        if (pattern.test(inputs[0].value)) {
            inputs[0].classList.add("green");
            setTimeout(()=>{
                document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
            },500)
        
        } else {
            if (document.querySelector("h2 div")) {     
            } else {
                document.querySelectorAll("h2")[1].append(document.createElement("div")); 
                document.querySelector("h2 div").innerText = "Помилка";            
            }

        }
    })

/*
Слайдер
Створіть слайдер кожні 3 сек змінюватиме зображення
*/

    let slider = document.querySelector(".slider"); 
    let counter = 0;
    let images = [
        'https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg',
        'https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg',
        'https://historyurok.com.ua/wp-content/uploads/2018/08/Zovnishnij-viglyad-1.jpg',
        'https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg',
        'https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg'  
    ]

    setInterval(() => {
        slider.src = images[counter++ % images.length];
    }, 3000);



})


