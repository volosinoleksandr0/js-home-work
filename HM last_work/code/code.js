let flag = false;
function showShoping() {

    if (flag) {
        flag = false;
        document.querySelector(".modal").classList.add("nonvisibl");
        document.querySelector(".modal").classList.remove("visibl");        
    } else {
        flag = true;
        document.querySelector(".modal").classList.add("visibl");
        document.querySelector(".modal").classList.remove("nonvisibl");        
    }
    document.querySelector("#shopping-list").innerHTML="";
    let shopingArr = JSON.parse(localStorage.getItem('buyItem')); //получили масив всех покупок
    let filtredShoping = {};//созд об для вівода ид каждой покупки и ее кол-ва
    for (const item of shopingArr) {//цикл перебир все ел-ті из масива покупок
        filtredShoping[item] = filtredShoping[item] ? filtredShoping[item] + 1 : 1;//в об запис ид предмета (item) и кол-во его повторений (filtredShoping[item])
    } 
    for (const key in filtredShoping) {//перебираем отфильтров покупки (по количеству ид)
        let curentItem = data.filter((e)=>{return e.id == key});//по ид находим нужн товар
        document.querySelector("#shopping-list").insertAdjacentHTML("beforeend",//запис его свойства в табл + количество посчитаное ранее + общ сума 
        `
        <tr>
            <td><img src="catalog/img/${curentItem[0].Img}" alt=""></td>
            <td>${curentItem[0].Name}</td>
            <td>${curentItem[0].Price}</td>
            <td>${filtredShoping[key]}</td>
            <td>${filtredShoping[key]*+curentItem[0].Price}</td>
        </tr>
        `)
    }  
}

document.querySelector(".shopping-cart").addEventListener("click",showShoping);

function getRandom() {
    let randomIdArr = [];
    for (let index = 0; randomIdArr.length < 4; index++) {
        randomId = Math.floor((Math.random() * 49) + 1);
        if (randomIdArr.includes(randomId)) {
        } else {
            randomIdArr.push( randomId);
        }

    }
   console.dir(randomIdArr);
   return randomIdArr;
    
}

function buyingItem(item) {//ф-ц для дод товара в корзину. Импорт/експорт не робит Почему?
    let buyedId = JSON.parse(localStorage.getItem('buyItem'));//получили масив уже купл товаров
    if (buyedId) { //если он есть
        buyedId.push(item);//добавили нов товар
        localStorage.setItem('buyItem', JSON.stringify(buyedId));//отправ в лок сторедж
    } else { //если нету
        localStorage.setItem('buyItem', JSON.stringify([item]));  //создали его  
    }
} 

function showRand() {
    let randomItems = getRandom();
    for (let index = 0; index < randomItems.length; index++) {
        let randomItem = data.filter((e)=>{return e.id == randomItems[index]});//по ид находим нужн товар
        document.querySelector("#random-container").insertAdjacentHTML("beforeend",// 
        `
        <div class="goods">
        <img class="goods-img" src="catalog/img/${randomItem[0].Img}" alt="">
        <p value=${randomItem[0].id}>${randomItem[0].Name} id:${randomItem[0].id}</p> 
        <div class="star-box" value=${randomItem[0].Stars}></div> 
        <p>As low as $${randomItem[0].Price}</p>
        <div class="color-box" >${randomItem[0].Colors}</div>
        <p>Sizes ${randomItem[0].Size}</p>
        <div class="add-button">
            <img src="img/add.png" alt="">
            ADD TO CART
        </div>
        </div>
        `)/**/
        console.dir(randomItem);
    }
    let starBoxs = document.querySelectorAll(".star-box"); //берем все див для звезд в карточках товаров
    for (let star = 0; star < starBoxs.length; star++) { //перебираем их
        for (let index = 0; index < starBoxs[star].attributes[1].value; index++) { //кол-во звезд сравниваем по валие в теге
            starBoxs[star].innerHTML+=(`<img src="img/Star_1.png" alt=""></img>`); // дод звезды в кажд из них 
            
        } 
    }

    let colorBoxs = document.querySelectorAll(".color-box");//берем все диві для цветов
    for (let colorBox = 0; colorBox < colorBoxs.length; colorBox++) {//перебираем их
        let colors = colorBoxs[colorBox].outerText.split(",");//с каждого берем текстовой список іветов конкретн товара и засов его в масив
        colorBoxs[colorBox].innerText = "";//удаляем текстовой список
        for (let index = 0; index < colors.length; index++) {//перебираем цвета с масива
            switch (colors[index]) {                //в див для цветов добавл цвет котор соответсвует фналогичному с масива
                case "black":                   //таким образом текст переобраз в изображ
                    colorBoxs[colorBox].innerHTML+=(`<div class="black"></div>`)
                    break;
                case "brown":
                    colorBoxs[colorBox].innerHTML+=(`<div class="brown"></div>`)
                    break;            
                case "green":
                    colorBoxs[colorBox].innerHTML+=(`<div class="green"></div>`)
                    break;
                case "grey":
                    colorBoxs[colorBox].innerHTML+=(`<div class="grey"></div>`)
                    break; 
                case "yellow":
                    colorBoxs[colorBox].innerHTML+=(`<div class="yellow"></div>`)
                    break;
                case "white":
                    colorBoxs[colorBox].innerHTML+=(`<div class="white"></div>`)
                    break; 
                case "blue":
                    colorBoxs[colorBox].innerHTML+=(`<div class="blue"></div>`)
                    break; 
                    default:
                    break;
            }
        }
        
    }
    
    for (let index = 0; index < document.querySelectorAll(".goods").length; index++) {//вібрал все карточки товаров на єкране и перебираем их
        document.querySelectorAll(".goods-img")[index].addEventListener("click",(e)=>{  //на каждую вешаем собітие клика
            //alert(e.currentTarget.children[1].attributes[0].nodeValue);//e.currentTarget - выбраный див. nextElementSibling.attributes[0].nodeValue - место в верстке куда сохр ид товара
            localStorage.setItem('chosenId', e.currentTarget.nextElementSibling.attributes[0].nodeValue);//по клику в локсторедж создается перемен chosenId куда сохр ид вібраного товара
            location.assign("infopage/index.html");//переход на друг стр
        }) 
          
    }  
    
    for (let index = 0; index < document.querySelectorAll(".goods .add-button").length; index++) {//вібрал все buttons c карточки товаров на єкране и перебираем их
        document.querySelectorAll(".goods .add-button")[index].addEventListener("click",(e)=>{  //на каждую вешаем собітие клика
            //alert("e.currentTarget.parentNode.children[1].attributes[0].nodeValue");//e.currentTarget.parentNode - див нажатой кнпк. children[1].attributes[0].nodeValue - место в верстке куда сохр ид товара
            buyingItem(e.currentTarget.parentNode.children[1].attributes[0].nodeValue);//запуск фнкц покупки
            alert("Item added");
        }) 
          
    }
    
}

showRand();