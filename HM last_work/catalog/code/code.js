
let objJson = [];
let flag = false;
let btnNext = document.getElementById("btn_next");
let btnPrev = document.getElementById("btn_prev");
let listing_table = document.getElementById("listingTable");
let page_span = document.getElementById("page");

let current_page = 1;
let records_per_page = 10;

function getList() {
    pages.innerHTML="";
    let paginationPages = [];
    for(let i = 1 ; i <= numPages(); i++){
        let li = document.createElement('li')
        li.innerText = i;
        pages.insertAdjacentElement('beforeend',li)
        paginationPages.push(li)
    }
    paginationPages.forEach((element) => element.addEventListener("click",
        (e)=>{
            changePage(e.target.innerText);
            current_page = e.target.innerText;
        }
    ))
    
}


function prevPage()
{
    if (current_page > 1) {
        current_page--;
        changePage(current_page);
    }
}

function nextPage()
{
    if (current_page < numPages()) {
        current_page++;
        changePage(current_page);
    }
}

function changePage(page)
{
    // Validate page
    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();

    listing_table.innerHTML = "";

    for (let i = (page-1) * records_per_page; i < (page*records_per_page) && i < objJson.length; i++) { //1->10
        listing_table.innerHTML +=             /* указ в валуе сколько звезд у конкретного товара*/                 
        `<div class="goods">
        <img class="goods-img" src="img/${objJson[i].Img}" alt="">
        <p value=${objJson[i].id}>${objJson[i].Name} id:${objJson[i].id}</p> 
        <div class="star-box" value=${objJson[i].Stars}></div> 
        <p>As low as $${objJson[i].Price}</p>
        <div class="color-box" >${objJson[i].Colors}</div>
        <p>Sizes ${objJson[i].Size}</p>
        <div class="add-button">
            <img src="img/add.png" alt="">
            ADD TO CART
        </div>
    </div>`;

    }
    let starBoxs = document.querySelectorAll(".star-box"); //берем все див для звезд в карточках товаров
    for (let star = 0; star < starBoxs.length; star++) { //перебираем их
        for (let index = 0; index < starBoxs[star].attributes[1].value; index++) { //кол-во звезд сравниваем по валие в теге
            starBoxs[star].innerHTML+=(`<img src="img/Star_1.png" alt=""></img>`); // дод звезды в кажд из них 
            
        } 
    }

    let colorBoxs = document.querySelectorAll(".color-box");//берем все диві для цветов
    for (let colorBox = 0; colorBox < colorBoxs.length; colorBox++) {//перебираем их
        let colors = colorBoxs[colorBox].outerText.split(",");//с каждого берем текстовой список іветов конкретн товара и засов его в масив
        colorBoxs[colorBox].innerText = "";//удаляем текстовой список
        for (let index = 0; index < colors.length; index++) {//перебираем цвета с масива
            switch (colors[index]) {                //в див для цветов добавл цвет котор соответсвует фналогичному с масива
                case "black":                   //таким образом текст переобраз в изображ
                    colorBoxs[colorBox].innerHTML+=(`<div class="black"></div>`)
                    break;
                case "brown":
                    colorBoxs[colorBox].innerHTML+=(`<div class="brown"></div>`)
                    break;            
                case "green":
                    colorBoxs[colorBox].innerHTML+=(`<div class="green"></div>`)
                    break;
                case "grey":
                    colorBoxs[colorBox].innerHTML+=(`<div class="grey"></div>`)
                    break; 
                case "yellow":
                    colorBoxs[colorBox].innerHTML+=(`<div class="yellow"></div>`)
                    break;
                case "white":
                    colorBoxs[colorBox].innerHTML+=(`<div class="white"></div>`)
                    break; 
                case "blue":
                    colorBoxs[colorBox].innerHTML+=(`<div class="blue"></div>`)
                    break; 
                    default:
                    break;
            }
        }
        
    }
    
    for (let index = 0; index < document.querySelectorAll(".goods").length; index++) {//вібрал все карточки товаров на єкране и перебираем их
        document.querySelectorAll(".goods-img")[index].addEventListener("click",(e)=>{  //на каждую вешаем собітие клика
            //alert(e.currentTarget.children[1].attributes[0].nodeValue);//e.currentTarget - выбраный див. nextElementSibling.attributes[0].nodeValue - место в верстке куда сохр ид товара
            localStorage.setItem('chosenId', e.currentTarget.nextElementSibling.attributes[0].nodeValue);//по клику в локсторедж создается перемен chosenId куда сохр ид вібраного товара
            location.assign("../infopage/index.html");//переход на друг стр
        }) 
          
    }    

    for (let index = 0; index < document.querySelectorAll(".goods .add-button").length; index++) {//вібрал все buttons c карточки товаров на єкране и перебираем их
        document.querySelectorAll(".goods .add-button")[index].addEventListener("click",(e)=>{  //на каждую вешаем собітие клика
            //alert("e.currentTarget.parentNode.children[1].attributes[0].nodeValue");//e.currentTarget.parentNode - див нажатой кнпк. children[1].attributes[0].nodeValue - место в верстке куда сохр ид товара
            buyingItem(e.currentTarget.parentNode.children[1].attributes[0].nodeValue);//запуск фнкц покупки
            alert("Item added");
        }) 
          
    }
    

    page_span.innerText = page;

    if (page == 1) {
        btn_prev.classList.add("hidden");
        btn_prev.classList.remove("visible");
    } else {
        btn_prev.classList.add("visible");
        btn_prev.classList.remove("hidden");
    }

    if (page == numPages()) {
        btn_next.classList.add("hidden");
        btn_next.classList.remove("visible");
    } else {
        btn_next.classList.add("visible");
        btn_next.classList.remove("hidden");    
    }
}

function numPages()
{
    return Math.ceil(objJson.length / records_per_page);
}

btnNext.addEventListener("click",()=>{nextPage()});
btnPrev.addEventListener("click",()=>{prevPage()});

const getData = function (param) { 
        objJson = param;  //json v masiv
        changePage(1);  //otkr str 1
        getList();      //delaem str vnizy

    
}


function dataFilter(target, targrtType) {//ф-ц для поиска в масиве товаров принимает 2 аргум что конкретно искать и тип поиска
    switch (targrtType) { //тип 3 типов цвет размер и текст(по имени товара)
        case "color"://тип цвет
            if (target == undefined) {//если кликнули не на цвет а на простр между кнопками
                break;
            } else {
                if (target == "all") {//если выбрал все загруж неотфильтров масив товаров
                    newData = data;
                } else {
                    newData = data.filter((e)=>{return e.Colors.includes(target)});//иначе загр отфильтр
                }
                
            }
           
           current_page = 1;//очищ счетчик текущих стр
           getData(newData);//заново запуск осн ф-ц построен с новым даным
           break;           
        case "size"://тип размер
            if (target === "") {//кликнули не накнопку а на пустое пространство
                break;
            } else {
                if (target == "all") {//если выбрал все загруж неотфильтров масив товаров
                    newData = data;
                } else {//иначе загр отфильтр
                    newData = data.filter((e)=>{return e.Size.includes(+target)});  //таргет - текст, а в дата масив размеров -числа
                }             
            }
            current_page = 1;//очищ счетчик текущих стр
            getData(newData);//заново запуск осн ф-ц построен с новым даным
            break;    
        case "search"://тип текст
            newData = data.filter((e)=>{return e.Name.toLowerCase().match(target.toLowerCase())});//сразу формиров новый масив. //target.toLowerCase() - переобр любой текст поиска в нижн регистр e.Name.toLowerCase() -тоже для назв товара match -метод соответсв
            current_page = 1;//очищ счетчик текущих стр
            if (newData.length == 0 ) {//если в новом масиве ничего не оказалось
                alert("Совпадений не найдено");
                getData(data);//загруж неотфильтров масив товаров
            } else {
                getData(newData);
            }
            break;
        default:
            getData(data);
            break;
    }
    
}/**/

document.querySelector(".search-color .boxs-wrapper").addEventListener("click", (e)=>{ //выбр весь див контейнер с цветами и повесили событие клика
    dataFilter(e.target.classList[1],"color"); //ф-ц для поиска в масиве товаров
}) 

document.querySelector(".search-size .boxs-wrapper").addEventListener("click", (e)=>{ //выбр весь див контейнер с размерами и повесили событие клика
    dataFilter(e.target.attributes[1].value,"size");//ф-ц для поиска в масиве товаров
})

document.querySelector("#search").addEventListener("input",(ev)=>{ //выбр главн инпут и повесили событие ввода
    dataFilter(ev.currentTarget.value,"search");//ф-ц для поиска в масиве товаров
})

function buyingItem(item) {//ф-ц для дод товара в корзину. Импорт/експорт не робит Почему?
    let buyedId = JSON.parse(localStorage.getItem('buyItem'));//получили масив уже купл товаров
    if (buyedId) { //если он есть
        buyedId.push(item);//добавили нов товар
        localStorage.setItem('buyItem', JSON.stringify(buyedId));//отправ в лок сторедж
    } else { //если нету
        localStorage.setItem('buyItem', JSON.stringify([item]));  //создали его  
    }
} 


getData(data); 

function showShoping() {

    if (flag) {
        flag = false;
        document.querySelector(".modal").classList.add("nonvisibl");
        document.querySelector(".modal").classList.remove("visibl");        
    } else {
        flag = true;
        document.querySelector(".modal").classList.add("visibl");
        document.querySelector(".modal").classList.remove("nonvisibl");        
    }
    document.querySelector("#shopping-list").innerHTML="";
    let shopingArr = JSON.parse(localStorage.getItem('buyItem')); //получили масив всех покупок
    let filtredShoping = {};//созд об для вівода ид каждой покупки и ее кол-ва
    for (const item of shopingArr) {//цикл перебир все ел-ті из масива покупок
        filtredShoping[item] = filtredShoping[item] ? filtredShoping[item] + 1 : 1;//в об запис ид предмета (item) и кол-во его повторений (filtredShoping[item])
    } 
    for (const key in filtredShoping) {//перебираем отфильтров покупки (по количеству ид)
        let curentItem = data.filter((e)=>{return e.id == key});//по ид находим нужн товар
        document.querySelector("#shopping-list").insertAdjacentHTML("beforeend",//запис его свойства в табл + количество посчитаное ранее + общ сума 
        `
        <tr>
            <td><img src="../catalog/img/${curentItem[0].Img}" alt=""></td>
            <td>${curentItem[0].Name}</td>
            <td>${curentItem[0].Price}</td>
            <td>${filtredShoping[key]}</td>
            <td>${filtredShoping[key]*+curentItem[0].Price}</td>
        </tr>
        `)
    }  
}

document.querySelector(".shopping-cart").addEventListener("click",showShoping);

/*        case "color":
            if (target == "all") {
                newData = data;
            } else {
                newData = data.filter((e)=>{return e.Colors.includes(target)});
            }
           current_page = 1;//очищ счетчик текущих стр
           if (newData.length == 0 ) {
                getData(data);
            } else {
                getData(newData);
            }
            break;  */    