const data = 
[
    {
        "Name":"Sportif's Original Short 1",
        "Stars":"5",
        "Price":"34",
        "Colors":["black","brown","yellow","white"],   //1
        "Size":[30,32,34,36,38,40,42,44],
        "Img":"goods (1).png",
        "id":1
    },
    {
        "Name":"Sportif's Hatteras Short 1",
        "Stars":"4",
        "Price":"32",   //2
        "Colors":["black","green","grey","yellow","white"],
        "Size":[30,32,34],
        "Img":"goods (2).png",
        "id":2
    },
    {
        "Name":"Sportif's Tidewater Short 1",
        "Stars":"4",
        "Price":"56",  //3
        "Colors":["black","brown","blue","green","grey"],
        "Size":[36,32,34,38],
        "Img":"goods (3).png",
        "id":3
    },
    {
        "Name":"Sportif's Lauderdale Short 1",
        "Stars":"3",
        "Price":"45",    //4
        "Colors":["brown","blue","green","grey","yellow"],
        "Size":[30,34,38],
        "Img":"goods (4).png",
        "id":4
    },
    {
        "Name":"Captain's Short 1",
        "Stars":"4",
        "Price":"31",
        "Colors":["black","green","yellow","white"],    //5
        "Size":[30,40,42,44],
        "Img":"goods (5).png",
        "id":5
    },    
    {
        "Name":"Sportif's Original Short 2",
        "Stars":"3",
        "Price":"21",
        "Colors":["black","brown","green","grey","white"],   //1
        "Size":[30,32,40,42,44],
        "Img":"goods (6).png",
        "id":6
    },
    {
        "Name":"Sportif's Hatteras Short 2",
        "Stars":"2",
        "Price":"22",   //2
        "Colors":["black","yellow","white"],
        "Size":[30,40,42,44],
        "Img":"goods (7).png",
        "id":7
    },
    {
        "Name":"Sportif's Tidewater Short 2",
        "Stars":"5",
        "Price":"31",  //3
        "Colors":["brown","blue","green","grey","yellow"],
        "Size":[32,34,36,38,40,42],
        "Img":"goods (8).png",
        "id":8
    },
    {
        "Name":"Sportif's Lauderdale Short 2",
        "Stars":"4",
        "Price":"51",    //4
        "Colors":["black","blue","green","grey","white"],
        "Size":[30,32,36,38,42,44],
        "Img":"goods (9).png",
        "id":9
    },
    {
        "Name":"Captain's Short 2",
        "Stars":"5",
        "Price":"47",
        "Colors":["black","brown","green","grey","yellow","white"],    //5
        "Size":[30,32,36,38,40,42],
        "Img":"goods (10).png",
        "id":10
    },    
    {
        "Name":"Sportif's Original Short 3",
        "Stars":"4",
        "Price":"56",
        "Colors":["black","brown","green","grey","yellow","white"],   //1
        "Size":[30,32,34,36,38,42],
        "Img":"goods (11).png",
        "id":49
    },
    {
        "Name":"Sportif's Hatteras Short 3",
        "Stars":"3",
        "Price":"39",   //2
        "Colors":["black","brown","green","yellow","white"],
        "Size":[30,32,38,40,42],
        "Img":"goods (1).png",
        "id":11
    },
    {
        "Name":"Sportif's Tidewater Short 3",
        "Stars":"5",
        "Price":"36",  //3
        "Colors":["brown","green","grey","yellow","white"],
        "Size":[30,32,34,36,38,40,44],
        "Img":"goods (2).png",
        "id":12
    },
    {
        "Name":"Sportif's Lauderdale Short 3",
        "Stars":"4",
        "Price":"38",    //4
        "Colors":["black","green","grey","yellow","white"],
        "Size":[30,32,34,42,44],
        "Img":"goods (3).png",
        "id":13
    },
    {
        "Name":"Captain's Short 3",
        "Stars":"3",
        "Price":"42",
        "Colors":["black","brown","blue","green","grey","yellow","white"],    //5
        "Size":[30,32,34,36,38,40,42,44],
        "Img":"goods (4).png",
        "id":14
    },    
    {
        "Name":"Sportif's Original Short 4",
        "Stars":"4",
        "Price":"44",
        "Colors":["black","brown","blue","yellow","white"],   //1
        "Size":[30,32,34,36,38],
        "Img":"goods (5).png",
        "id":15
    },
    {
        "Name":"Sportif's Hatteras Short 4",
        "Stars":"5",
        "Price":"51",   //2
        "Colors":["black","brown","white"],
        "Size":[30,32,34],
        "Img":"goods (6).png",
        "id":16
    },
    {
        "Name":"Sportif's Tidewater Short 4",
        "Stars":"5",
        "Price":"61",  //3
        "Colors":["black","blue","grey","white"],
        "Size":[30,34,36,42,44],
        "Img":"goods (7).png",
        "id":17
    },
    {
        "Name":"Sportif's Lauderdale Short 4",
        "Stars":"4",
        "Price":"39",    //4
        "Colors":["black","brown","blue","green"],
        "Size":[30,32,34,36,38],
        "Img":"goods (8).png",
        "id":18
    },
    {
        "Name":"Captain's Short 4",
        "Stars":"3",
        "Price":"29",
        "Colors":["green","grey","yellow","white"],    //5
        "Size":[38,40,42,44],
        "Img":"goods (9).png",
        "id":19
    },    
    {
        "Name":"Sportif's Original Short 5",
        "Stars":"2",
        "Price":"31",
        "Colors":["black","blue","yellow","white"],   //1
        "Size":[30,32,34,40,42],
        "Img":"goods (10).png",
        "id":50
    },
    {
        "Name":"Sportif's Hatteras Short 5",
        "Stars":"1",
        "Price":"41",   //2
        "Colors":["black","brown","blue","green","grey"],
        "Size":[30,32,34,36,42,44],
        "Img":"goods (11).png",
        "id":20
    },
    {
        "Name":"Sportif's Tidewater Short 5",
        "Stars":"5",
        "Price":"19",  //3
        "Colors":["black","brown","blue"],
        "Size":[30,32,34,36,44],
        "Img":"goods (1).png",
        "id":21
    },
    {
        "Name":"Sportif's Lauderdale Short 5",
        "Stars":"4",
        "Price":"37",    //4
        "Colors":["black",,"green","grey","yellow","white"],
        "Size":[30,36,38,40,42,44,46],
        "Img":"goods (2).png",
        "id":22
    },
    {
        "Name":"Captain's Short 5",
        "Stars":"3",
        "Price":"26",
        "Colors":["black","blue","grey","yellow","white"],    //5
        "Size":[30,32,34,36,38,40],
        "Img":"goods (3).png",
        "id":23
    },    
    {
        "Name":"Sportif's Original Short 6",
        "Stars":"4",
        "Price":"48",
        "Colors":["black","brown","blue","green","yellow","white"],   //1
        "Size":[30,32,36,38,40,42,44],
        "Img":"goods (4).png",
        "id":24
    },
    {
        "Name":"Sportif's Hatteras Short 6",
        "Stars":"5",
        "Price":"35",   //2
        "Colors":["black","brown","blue","grey","yellow","white"],
        "Size":[30,34,36,38,40,42,44],
        "Img":"goods (5).png",
        "id":25
    },
    {
        "Name":"Sportif's Tidewater Short 6",
        "Stars":"5",
        "Price":"44",  //3
        "Colors":["black","blue","green","grey","yellow"],
        "Size":[30,32,36,40,42,44],
        "Img":"goods (6).png",
        "id":26
    },
    {
        "Name":"Sportif's Lauderdale Short 6",
        "Stars":"4",
        "Price":"54",    //4
        "Colors":["black","brown","grey","white"],
        "Size":[30,32,38,40],
        "Img":"goods (7).png",
        "id":27
    },
    {
        "Name":"Captain's Short 6",
        "Stars":"3",
        "Price":"62",
        "Colors":["brown","blue","green","grey","yellow"],    //5
        "Size":[32,34,36,38,40,42],
        "Img":"goods (8).png",
        "id":28
    },    
    {
        "Name":"Sportif's Original Short 7",
        "Stars":"2",
        "Price":"67",
        "Colors":["blue","green","grey","white"],   //1
        "Size":[30,34,36,40,42],
        "Img":"goods (9).png",
        "id":29
    },
    {
        "Name":"Sportif's Hatteras Short 7",
        "Stars":"5",
        "Price":"53",   //2
        "Colors":["black","green","yellow","white"],
        "Size":[30,32,34,42,44],
        "Img":"goods (10).png",
        "id":30
    },
    {
        "Name":"Sportif's Tidewater Short 7",
        "Stars":"4",
        "Price":"36",  //3
        "Colors":["black","green","grey","yellow","white"],
        "Size":[30,32,34,44],
        "Img":"goods (11).png",
        "id":31
    },
    {
        "Name":"Sportif's Lauderdale Short 7",
        "Stars":"3",
        "Price":"56",    //4
        "Colors":["black","grey","yellow","white"],
        "Size":[30,44],
        "Img":"goods (1).png",
        "id":32
    },
    {
        "Name":"Captain's Short 7",
        "Stars":"3",
        "Price":"45",
        "Colors":["black","white"],    //5
        "Size":[30,32,34,36],
        "Img":"goods (2).png",
        "id":33
    },   
    {
        "Name":"Sportif's Original Short 8",
        "Stars":"4",
        "Price":"51",
        "Colors":["black","green","grey","yellow","white"],   //1
        "Size":[30,32,34,42,44],
        "Img":"goods (3).png",
        "id":34
    },
    {
        "Name":"Sportif's Hatteras Short 8",
        "Stars":"5",
        "Price":"52",   //2
        "Colors":["blue","green","grey","yellow","white"],
        "Size":[30,36,38,40,42,44],
        "Img":"goods (4).png",
        "id":35
    },
    {
        "Name":"Sportif's Tidewater Short 8",
        "Stars":"4",
        "Price":"53",  //3
        "Colors":["brown","blue","green","grey"],
        "Size":[30,32,34,36,38],
        "Img":"goods (5).png",
        "id":36
    },
    {
        "Name":"Sportif's Lauderdale Short 8",
        "Stars":"5",
        "Price":"78",    //4
        "Colors":["blue","green","yellow","white"],
        "Size":[30,32,42,44],
        "Img":"goods (6).png",
        "id":37
    },
    {
        "Name":"Captain's Short 8",
        "Stars":"4",
        "Price":"32",
        "Colors":["black","green","grey","yellow","white"],    //5
        "Size":[30,32,34,42,44],
        "Img":"goods (7).png",
        "id":38
    },    
    {
        "Name":"Sportif's Original Short 9",
        "Stars":"3",
        "Price":"65",
        "Colors":["black","brown","green","grey","white"],   //1
        "Size":[30,32,34,36,44],
        "Img":"goods (8).png",
        "id":39
    },
    {
        "Name":"Sportif's Hatteras Short 9",
        "Stars":"5",
        "Price":"67",   //2
        "Colors":["green","grey","yellow","white"],
        "Size":[30,32,34,36,38],
        "Img":"goods (9).png",
        "id":40
    },
    {
        "Name":"Sportif's Tidewater Short 9",
        "Stars":"4",
        "Price":"37",  //3
        "Colors":["black","brown","blue","yellow","white"],
        "Size":[30,32,38,40,42,44],
        "Img":"goods (10).png",
        "id":41
    },
    {
        "Name":"Sportif's Lauderdale Short 9",
        "Stars":"4",
        "Price":"36",    //4
        "Colors":["black","brown","white"],
        "Size":[30,32,34,36,38],
        "Img":"goods (11).png",
        "id":42
    },
    {
        "Name":"Captain's Short 9",
        "Stars":"5",
        "Price":"35",
        "Colors":["black","grey","yellow","white"],    //5
        "Size":[30,32,34,44],
        "Img":"goods (1).png",
        "id":43
    },    
    {
        "Name":"Sportif's Original Short 10",
        "Stars":"3",
        "Price":"42",
        "Colors":["black","brown","blue","white"],   //1
        "Size":[30,34,36,38,],
        "Img":"goods (2).png",
        "id":44
    },
    {
        "Name":"Sportif's Hatteras Short 10",
        "Stars":"3",
        "Price":"43",   //2
        "Colors":["black","brown","yellow","white"],
        "Size":[30,32,34,42,44],
        "Img":"goods (3).png",
        "id":45
    },
    {
        "Name":"Sportif's Tidewater Short 10",
        "Stars":"2",
        "Price":"48",  //3
        "Colors":["black","brown","yellow","white"],
        "Size":[30,32,34,42,44],
        "Img":"goods (4).png",
        "id":46
    },
    {
        "Name":"Sportif's Lauderdale Short 10",
        "Stars":"5",
        "Price":"47",    //4
        "Colors":["green","grey","yellow","white"],
        "Size":[30,40,42,44],
        "Img":"goods (5).png",
        "id":47
    },
    {
        "Name":"Captain's Short 10",
        "Stars":"4",
        "Price":"60",
        "Colors":["black","brown","blue","white"],    //5
        "Size":[30,32,34,36,38],
        "Img":"goods (6).png",
        "id":48
    },
]
