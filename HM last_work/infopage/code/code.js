let flag = false;
function loadData() {
    const id = localStorage.getItem('chosenId'); //получ ид вібраного товара
    let chosenItem = data.filter((e)=>{return e.id == id}); //по ид находим вібран товар (chosenItem масив на 1 ел-т)
    document.querySelector(".description h2").innerText = `${chosenItem[0].Name} id:${chosenItem[0].id}`;// с найденого вібраного товара отображ на стр имя + ид
    document.querySelector(".price").innerText =`$${chosenItem[0].Price}`;// с найденого вібраного товара отображ на стр цену
    document.querySelector(".screen").innerHTML = `<img src="../catalog/img/${chosenItem[0].Img}" alt="">`;// с найденого вібраного товара отображ на стр фото товара

    for (let index = 0; index < chosenItem[0].Colors.length; index++) {//перебираем масив цветов вібран товар
        document.querySelectorAll(".container")[1].innerHTML +=   //нах на єкране див контейнер для цветов в котор запис на каждой итерации                           
        `<div class="size-box ${chosenItem[0].Colors[index]}"></div>`;// с найденого вібраного товара отображ на стр цвета товара
    }
    
    for (let index = 0; index < chosenItem[0].Stars; index++) {//цикл раб столько раз сколько = оценка товара
        document.querySelectorAll(".container")[0].innerHTML += //нах на єкране див контейнер для звезд в котор запис на каждой итерации                             
        `<img src="img/Star_1.png" alt="">`;// с найденого вібраного товара отображ на стр оценк товара
    }

    for (let index = 0; index < chosenItem[0].Size.length; index++) {//перебираем масив размеров вібран товар
        document.querySelector(".size-boxs-wrapper").innerHTML +=   //нах на єкране див контейнер для размера в котор запис на каждой итерации                        
        `<div class="size-box">${chosenItem[0].Size[index]}</div>`;// с найденого вібраного товара отображ на стр размер товара
    }

    


    buyingItem(id);

}


loadData();//ф-ц загруз на єкран даніе вібраного товара

// скопир в каталог там 12 и ид поменять на таргет по клику на кнопку на карточке
function buyingItem(item) {
    for (let index = 0; index < document.querySelectorAll(".add-buttons div").length; index++) {
        document.querySelectorAll(".add-buttons div")[index].addEventListener("click",()=>{
            let buyedId = JSON.parse(localStorage.getItem('buyItem'));//получили масив уже купл товаров
            if (buyedId) { //если он есть
                buyedId.push(item);//добавили нов товар
                localStorage.setItem('buyItem', JSON.stringify(buyedId));//отправ в лок сторедж
            } else { //если нету
                localStorage.setItem('buyItem', JSON.stringify([item]));  //создали его  
            }
        alert("Item added");
        })
        
    }
}  

function showShoping() {

    if (flag) {
        flag = false;
        document.querySelector(".modal").classList.add("nonvisible");
        document.querySelector(".modal").classList.remove("visible");        
    } else {
        flag = true;
        document.querySelector(".modal").classList.add("visible");
        document.querySelector(".modal").classList.remove("nonvisible");        
    }
    document.querySelector("#shopping-list").innerHTML="";
    let shopingArr = JSON.parse(localStorage.getItem('buyItem')); //получили масив всех покупок
    let filtredShoping = {};//созд об для вівода ид каждой покупки и ее кол-ва
    for (const item of shopingArr) {//цикл перебир все ел-ті из масива покупок
        filtredShoping[item] = filtredShoping[item] ? filtredShoping[item] + 1 : 1;//в об запис ид предмета (item) и кол-во его повторений (filtredShoping[item])
    } 
    for (const key in filtredShoping) {
        let curentItem = data.filter((e)=>{return e.id == key});
        document.querySelector("#shopping-list").insertAdjacentHTML("beforeend",
        `
        <tr>
            <td><img src="../catalog/img/${curentItem[0].Img}" alt=""></td>
            <td>${curentItem[0].Name}</td>
            <td>${curentItem[0].Price}</td>
            <td>${filtredShoping[key]}</td>
            <td>${filtredShoping[key]*+curentItem[0].Price}</td>
        </tr>
        `)
    }  
}

document.querySelector(".shopping-cart").addEventListener("click",showShoping);
/* использ ф-ц єту в корзине для подсчета кол-ва товаров

Для нахождения одинаковых элементов можно использовать следующий алгоритм:

Находим количество вхождений (сколько раз встречается в списке) для каждого элемента
Выводим только те, у которых количество вхождений больше 1
Алгоритм можно реализовать с помощью цикла:

const numbers = [4, 3, 3, 1, 15, 7, 4, 19, 19]; // исходный массив

const countItems = {}; // здесь будет храниться промежуточный результат

// получаем объект в котором ключ - это элемент массива, а значение - сколько раз встречается элемент в списке
// например так будет выглядеть этот объект после цикла:
// {1: 1, 3: 2, 4: 2, 7: 1, 15: 1, 19: 2}
// 1 встречается в тексте 1 раз, 2 встречается 2 раза, 4 встречается 2 раза и так далее
for (const item of numbers) {
  // если элемент уже был, то прибавляем 1, если нет - устанавливаем 1
  countItems[item] = countItems[item] ? countItems[item] + 1 : 1;
}

// обрабатываем ключи объекта, отфильтровываем все, что меньше 1
const result = Object.keys(countItems).filter((item) => countItems[item] > 1);
console.dir(result); // => ['3', '4', '19']

*/


