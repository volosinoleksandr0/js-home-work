/*

Реалізувати функцію створення об'єкта "користувач".
   
Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
При виклику функція повинна запитати у імені, що викликає, і прізвище.
Використовуючи дані, введені користувачем, створити об'єкт newUser з властивостями firstName та lastName.
Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені користувача, 
з'єднану з прізвищем користувача, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
Створити користувача за допомогою функції createNewUser(). Викликати користувача функцію getLogin(). 
Вивести у консоль результат виконання функції.


Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем.
Візьміть виконане завдання вище (створена вами функція createNewUser()) та доповніть її наступним функціоналом:

При виклику функція повинна запитати у дату народження, що викликає, (текст у форматі dd.mm.yyyy) 
і зберегти її в полі birthday.
Створити метод getAge() який повертатиме скільки користувачеві років.
Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі,
поєднану з прізвищем (у нижньому регістрі) та роком народження. 
(Наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).


Вивести в консоль результат роботи функції createNewUser()
, а також функцій getAge() та getPassword() створеного об'єкта.




*/

class createNewUser {
    constructor(){
        //свойства экземпляра
        this.firstName = prompt("Введіть ваше ім'я:", "Ivan" );
        this.lastName = prompt("Введіть ваше прізвище:", "Kravchenko");
        this.birthday = prompt("Введіть вашу дату народження (текст у форматі dd.mm.yyyy):", "13.03.1992");
        
    }
    
    //метод на прототипе
    getLogin(){
    	console.log(`Логін:${(this.firstName[0] + this.lastName).toLowerCase()}`);
    	return this.firstName[0] + this.lastName;   
    }

    getAge(){
    	const age = this.birthday.split('.'); // 0-dd 1-mm 2-yy 
    	let year = new Date().getFullYear() - age[2];
    	let month = new Date().getMonth() - age[1];
    	let day = new Date().getDate() - age[0];
    	if (month < 0 || (month == 0 && day < 0 )) {year--}
    	console.log(`Вік:${year}`);
    	return {year}
    	//return this.firstName[0] + this.lastName;   

    }

    getPassword(){
    	console.log(`Пароль:${this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.split('.')[2]}`);
    	return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.split('.')[2]);   
    }
}

let newUser = new createNewUser();
newUser.getLogin();
newUser.getAge();
newUser.getPassword();
/*Реалізувати функцію фільтру масиву за вказаним типом даних.
                   
Написати функцію filterBy(), яка прийматиме 2 аргументи. 
Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
Функція повинна повернути новий масив, який міститиме всі дані, 
які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. 
Тобто якщо передати масив ['hello', 'world', 23, '23', null], 
і другим аргументом передати 'string', то функція поверне масив [23, null].*/

function filterBy (arr,type) {
	let newArr = new Array();
	for (let i = 0; i < arr.length; i++) {
		if (typeof arr[i] == type) {
			delete arr[i];
		} 
		if (arr[i] !== undefined) {
			newArr.push(arr[i]);
		} 
	}
	return newArr;
}

let customArray = ['hello', 'world', 23, '23', null,'23vd','23s',12,null,56,'s2s3s',77,true];
document.write(customArray+"<br/>");
let newArray = filterBy(customArray,'string');
document.write(newArray+"<br/>");
/**/