window.addEventListener("DOMContentLoaded",()=>{
    const btn = document.querySelector(".keys"),
    display = document.querySelector(".display > input")
    
    let pointFlag = false;    
    let oper = "";
    
    function remember(m) {
        if (m) {
            calc.memory = display.value;
        } else {
            calc.memory = -display.value;
        }
        document.querySelector(".m").style.zIndex = 1;
        
    }

    btn.addEventListener("click", function (e) {
        let pressedBtn = e.target.value;
        switch (true) {
            case /mrc/i.test(pressedBtn): //mrc
                if (calc.memory == display.value) {
                    calc.memory = "";
                    document.querySelector(".m").style.zIndex = -1;
                }
                show(calc.memory, display); 
                calc.value1 = display.value;
                break; 
            case /m\+/i.test(pressedBtn): //m+
                remember(true); 
                break;   
            case /m\-/i.test(pressedBtn): //m-
                remember(false); 
                break; 
            case /\d/.test(pressedBtn): //numbers
                if (pressedBtn == "0" && display.value == "0") { //only one zero
                } else {

                    calc.value1 += pressedBtn; 
                    show(calc.value1, display);                    
                }
                break;
            case /\+/.test(pressedBtn): //+
                if (calc.value2 != "" && display.value != "") {
                    calc.value1 = display.value;
                }
                if (calc.value2 != "" && calc.value1 != "") {
                    calc.result = math(calc.value2, calc.value1, "+")
                    /*calc.value2 = "";
                    calc.value1 = "";
                    pointFlag = false;*/
                    clear();
                    show(calc.result, display)
                }
                calc.value2 = calc.value1;
                calc.value1 = "";
                pointFlag = false;
                //show(calc.value1, display);
                oper = "+";
                break;
            case /\-/.test(pressedBtn): //-
                if (calc.value2 != "" && display.value != "") {
                    calc.value1 = display.value;
                }
                if (calc.value2 != "" && calc.value1 != "") {
                    calc.result = math(calc.value2, calc.value1, "-")
                    /*calc.value2 = "";
                    calc.value1 = "";
                    pointFlag = false;*/
                    clear();
                    show(calc.result, display)
                }
                calc.value2 = calc.value1;
                calc.value1 = "";
                pointFlag = false;
                //show(calc.value1, display);
                oper = "-";
                break;
            case /\*/.test(pressedBtn): //*
                if (calc.value2 != "" && display.value != "") {
                    calc.value1 = display.value;
                }
                if (calc.value2 != "" && calc.value1 != "") {
                    calc.result = math(calc.value2, calc.value1, "*")
                    /*calc.value2 = "";
                    calc.value1 = "";
                    pointFlag = false;*/
                    clear();
                    show(calc.result, display)
                }
            calc.value2 = calc.value1;
            calc.value1 = "";
            pointFlag = false;
            //show(calc.value1, display);
            oper = "*";
            break;
        case /\//.test(pressedBtn): ///
            if (calc.value2 != "" && display.value != "") {
                calc.value1 = display.value;
            }
            if (calc.value2 != "" && calc.value1 != "") {
                calc.result = math(calc.value2, calc.value1, "/")
                /*calc.value2 = "";
                calc.value1 = "";
                pointFlag = false;*/
                clear();
                show(calc.result, display)
            }                
            calc.value2 = calc.value1;
            calc.value1 = "";
            pointFlag = false;
            //show(calc.value1, display);
            oper = "/";
            break;
        case /=/.test(pressedBtn): //=
            if (calc.value2 != "" && display.value != "") {
                calc.value1 = display.value;
            }

            calc.result = math(calc.value2, calc.value1, oper)
            /*calc.value2 = "";
            calc.value1 = "";
            pointFlag = false;*/
            clear();
            show(calc.result, display)
            break;
        case /c/i.test(pressedBtn): //clear
            clear();
            calc.result = "";
            show("", display); 
            break;   
        case /\./.test(pressedBtn): //.
            if (pointFlag) {            //omly one point
            } else {
                if (calc.value1 == "") { //zero on the start
                    calc.value1 = 0;
                }
               calc.value1 += pressedBtn; 
               show(calc.value1, display);
               pointFlag = true;

            }
            break;
        default:
          break; 
      }



})
})

const calc = {
value1 : "",
value2 : "",
result : "",
memory : ""
}

function math(arg1, arg2, operand) {
    switch (operand) {
        case "+":
            return +arg1 + +arg2;
            
        case "-":
            return arg1 - arg2;
            
        case "*":
            return arg1 * arg2;
            
        case "/":
            return arg1 / arg2;
                        
        default:
            break;
}
}

function clear() {
    calc.value2 = "";
    calc.value1 = "";
    pointFlag = false;
}



function show (value, el) {
    el.value = value
}


/*
function sum(arg1, arg2) {
return +arg1 + +arg2;
};

function dif(arg1, arg2) {
return arg1 - arg2;
};

function mul(arg1, arg2) {
return arg1 * arg2;
};

function div(arg1, arg2) {
return arg1 / arg2;
};*/

            /*switch (oper) {
                case "+":
                    math(calc.value2, calc.value1, oper)
                    //calc.result = sum(calc.value2, calc.value1);
                    break;
                case "-":
                    math(calc.value2, calc.value1, )
                    //calc.result = dif(calc.value2, calc.value1);
                    break;
                case "*":
                    math(calc.value2, calc.value1, )
                    //calc.result = mul(calc.value2, calc.value1);
                    break;
                case "/":
                    math(calc.value2, calc.value1, )
                    //calc.result = div(calc.value2, calc.value1);
                    break;            
                default:
                    break;
            }*/