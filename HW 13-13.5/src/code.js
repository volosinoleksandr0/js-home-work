/*
    Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/
    https://jsonplaceholder.typicode.com/comments
    Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
    На сторінку виводити по 10 коментарів, у низу сторінки зробити поле пагінації (перемикання сторінок) при перемиканні
    сторінок показувати нові коментарі. 
    з коментарів виводити : 
    "id": 1,
    "name"
    "email"
    "body": 
*/
let objJson = [];

let btnNext = document.getElementById("btn_next");
let btnPrev = document.getElementById("btn_prev");
let listing_table = document.getElementById("listingTable");
let page_span = document.getElementById("page");

let current_page = 1;
let records_per_page = 10;

function getList() {
    let paginationPages = [];
    for(let i = 1 ; i <= numPages(); i++){
        let li = document.createElement('li')
        li.innerText = i;
        pages.insertAdjacentElement('beforeend',li)
        paginationPages.push(li)
    }
    paginationPages.forEach((element) => element.addEventListener("click",
        (e)=>{
            changePage(e.target.innerText);
            current_page = e.target.innerText;
        }
    ))
    
}


function prevPage()
{
    if (current_page > 1) {
        current_page--;
        changePage(current_page);
    }
}

function nextPage()
{
    if (current_page < numPages()) {
        current_page++;
        changePage(current_page);
    }
}

function changePage(page)
{
    // Validate page
    if (page < 1) page = 1;
    if (page > numPages()) page = numPages();

    listing_table.innerHTML = "";

    for (let i = (page) * records_per_page; i > ((page-1)*records_per_page) ; i--) { //1->10
        //let i = (page-1) * records_per_page; i < (page*records_per_page) && i < objJson.length; i++  10->1
        //listing_table.innerHTML += `<div class = "comment">"id": ${objJson[i].id}, "name": ${objJson[i].name}, "email": ${objJson[i].email}, <br> "body": ${objJson[i].body} </div>` 10->1
        listing_table.innerHTML += `<div class = "comment">"id": ${objJson[i-1].id}, "name": ${objJson[i-1].name}, "email": ${objJson[i-1].email}, <br> "body": ${objJson[i-1].body} </div>`; //1->10
        
        //listing_table.appendChild(document.createElement("div"))
        //document.querySelectorAll("#listingTable div").classList.add("comment");
        //insertAdjacentElement('beforeend',`<div class = "comment">"id": ${objJson[i].id}, "name": ${objJson[i].name}, "email": ${objJson[i].email}, <br> "body": ${objJson[i].body} </div>`)
        
    }
    page_span.innerText = page;

    if (page == 1) {
        btn_prev.classList.add("hidden");
        btn_prev.classList.remove("visible");
        //btn_prev.style.visibility = "hidden";
    } else {
        btn_prev.classList.add("visible");
        btn_prev.classList.remove("hidden");
        //btn_prev.style.visibility = "visible";
    }

    if (page == numPages()) {
        btn_next.classList.add("hidden");
        btn_next.classList.remove("visible");
        //btn_next.style.visibility = "hidden";
    } else {
        btn_next.classList.add("visible");
        btn_next.classList.remove("hidden");
        //btn_next.style.visibility = "visible";
    }
}

function numPages()
{
    return Math.ceil(objJson.length / records_per_page);
}

btnNext.addEventListener("click",()=>{nextPage()});
btnPrev.addEventListener("click",()=>{prevPage()});

const getData = function () {

    let quest = fetch("https://jsonplaceholder.typicode.com/comments"); //zapros
    quest.then((answer)=>{
        return answer.json();   //otvet v json
    }).then((inf)=>{
        objJson = inf;  //json v masiv
        changePage(1);  //otkr str 1
        getList();      //delaem str vnizy
        //
       //return inf; //ne rab. pochemy?
    }); 
    
    
}

getData(); 



